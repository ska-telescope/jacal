# CMakeLists.txt defining the jacal shared library

add_library(jacal SHARED
  daliuge/DaliugeApplication.h
  factory/ApplicationUtils.cc
  factory/ApplicationUtils.h
  factory/CalcNE.cc
  factory/CalcNE.h
  factory/DaliugeApplicationFactory.cc
  factory/DaliugeApplicationFactory.h
  factory/GenerateGridData.cc
  factory/GenerateGridData.h
  factory/GridInvert.cc
  factory/GridInvert.h
  factory/GridPredict.cc
  factory/GridPredict.h
  factory/ImageEquation.cc
  factory/ImageEquation.h
  factory/InitSpectralCube.cc
  factory/InitSpectralCube.h
  factory/Interface.cc
  factory/Interface.h
  factory/Ingest.cc
  factory/Ingest.h
  factory/JacalBPCalibrator.cc
  factory/JacalBPCalibrator.h
  factory/LoadNE.cc
  factory/LoadNE.h
  factory/LoadParset.cc
  factory/LoadParset.h
  factory/LoadVis.cc
  factory/LoadVis.h
  factory/MajorCycle.cc
  factory/MajorCycle.h
  factory/NESpectralCube.cc
  factory/NESpectralCube.h
  factory/NEUtils.cc
  factory/NEUtils.h
  factory/OutputParams.cc
  factory/OutputParams.h
  factory/ReduceNE.cc
  factory/ReduceNE.h
  factory/rename.h
  factory/RestoreSolver.cc
  factory/RestoreSolver.h
  factory/SolveNE.cc
  factory/SolveNE.h
  factory/SpectralCube.cc
  factory/SpectralCube.h
  # data accessors
  factory/OnDemandBufferDataAccessor.cc
  factory/OnDemandBufferDataAccessor.h
  # gridders
  factory/gridders/AProjectGridderBase.cc
  factory/gridders/AProjectGridderBase.tcc
  factory/gridders/AProjectGridderBase.h
  factory/gridders/AWProjectVisGridder.cc
  factory/gridders/AWProjectVisGridder.h
  factory/gridders/BoxVisGridder.cc
  factory/gridders/BoxVisGridder.h
  factory/gridders/GriddingInfo.cc
  factory/gridders/GriddingInfo.h
  factory/gridders/IVisGridder.cc
  factory/gridders/IVisGridder.h
  factory/gridders/SphFuncVisGridder.cc
  factory/gridders/SphFuncVisGridder.h
  factory/gridders/VisData.cc
  factory/gridders/VisData.h
  factory/gridders/VisInfo.cc
  factory/gridders/VisInfo.h
  factory/gridders/VisGridder.cc
  factory/gridders/VisGridder.h
  factory/gridders/VisGridderFactory.cc
  factory/gridders/VisGridderFactory.h
  factory/gridders/WDependentGridderBase.cc
  factory/gridders/WDependentGridderBase.h
  factory/gridders/WProjectVisGridder.cc
  factory/gridders/WProjectVisGridder.h
)

target_include_directories(jacal PUBLIC
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${YANDASOFT_INCLUDE_DIRS}
  ${DLG_INCLUDE_DIR}
)

target_link_libraries(jacal PUBLIC
  ${YANDASOFT_LIBRARIES}
  ${CASACORE_LIBRARIES}
  Boost::regex
  Boost::system
)

target_compile_definitions(jacal PUBLIC 
  HAVE_CASACORE3
  casa=casacore
)
if (ENABLE_FLOAT_IMAGE_PARAMS)
    target_compile_definitions(jacal PUBLIC
           ASKAP_FLOAT_IMAGE_PARAMS
    )
endif()

install (
TARGETS jacal
EXPORT ska-sdp-jacal-targets
RUNTIME DESTINATION bin
LIBRARY DESTINATION lib
ARCHIVE DESTINATION lib
LIBRARY PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
)

add_subdirectory(test)
