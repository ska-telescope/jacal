#!/bin/bash

try() {
	"$@"
	status=$?
	if [ $status -ne 0 ]; then
		echo "Command exited with status $status, aborting: $@" 1>&2
		exit 1
	fi
}
# Start managers
dlg nm -w . -v --no-dlm -l . &
nm_pid=$!
dlg dim -v -N 127.0.0.1 -l . &
dim_pid=$!
echo "NM/DIM PIDs: $nm_pid/$dim_pid"



basename="$1"
jacal_so="$2"

# Translate and submit the graph, wait until it finishes
sed "
s|%JACAL_SO%|$jacal_so|g
s|%PARSET%|$PWD/$basename.in|g
" "$basename.json" | dlg unroll-and-partition | dlg map | dlg submit -w

try kill $dim_pid
try wait $dim_pid
try kill $nm_pid
try wait $nm_pid
