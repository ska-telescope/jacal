/// @file ImageEquation.cc
///
/// @details Equation for discrete Fourier transform of an image using
/// gridding and FFTs, but with scope for flexibility (DFTs, IDG, etc.)
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "ImageEquation"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_ImageEquation() {
        return std::string("ImageEquation; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_ImageEquation()

#include <factory/ImageEquation.h>
#include <factory/gridders/VisGridder.h>
#include <factory/gridders/BoxVisGridder.h>
#include <factory/gridders/SphFuncVisGridder.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>

ASKAP_LOGGER(logger, ".ImageEquation");

//#include <fft/FFTWrapper.h>

#include <profile/AskapProfiler.h>

#include <scimath/fitting/Params.h>
#include <scimath/fitting/ImagingNormalEquations.h>
#include <scimath/fitting/DesignMatrix.h>
#include <scimath/fitting/Axes.h>

#include <measurementequation/SynthesisParamsHelper.h>

#include <casacore/scimath/Mathematics/RigidVector.h>

#include <casacore/casa/BasicSL/Constants.h>
#include <casacore/casa/BasicSL/Complex.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/ArrayMath.h>

#include <stdexcept>

using askap::scimath::Params;
using askap::scimath::Axes;
using askap::scimath::ImagingNormalEquations;
using askap::scimath::DesignMatrix;

namespace askap
{

    /// @brief default constructor
    ImageEquation::ImageEquation() {}

    /// Constructor with specified gridder and iterator
    /// @param ip Parameters
    /// @param idi Data iterator
    /// @param gridder Shared pointer to a gridder
    /// @param parset imager parameter set
    ImageEquation::ImageEquation(const askap::scimath::Params& ip,
                                 accessors::IDataSharedIter& idi,
                                 IVisGridder::ShPtr gridder,
                                 const LOFAR::ParameterSet& parset) :
        itsGridder(gridder), itsIdi(idi),
        itsSphFuncPSFGridder(false), itsBoxPSFGridder(false),
        itsUsePreconGridder(false), itsDataPartitionType("time")
    {
      setParameters(ip);
      init(parset);
    }

    /// Constructor without specifying gridder
    /// @param ip Parameters
    /// @param idi Data iterator
    /// @param parset imager parameter set
    ImageEquation::ImageEquation(const askap::scimath::Params& ip,
                                 accessors::IDataSharedIter& idi,
                                 const LOFAR::ParameterSet& parset) :
        itsIdi(idi), itsSphFuncPSFGridder(false), itsBoxPSFGridder(false),
        itsUsePreconGridder(false), itsDataPartitionType("time")
    {
      itsGridder = IVisGridder::ShPtr(new SphFuncVisGridder());
      setParameters(ip);
      init(parset);
    }

    /// Constructor without specifying iterator
    /// @param ip Parameters
    /// @param gridder Shared pointer to a gridder
    /// @param parset imager parameter set
    ImageEquation::ImageEquation(const askap::scimath::Params& ip,
                                 IVisGridder::ShPtr gridder,
                                 const LOFAR::ParameterSet& parset) :
        itsGridder(gridder),
        itsSphFuncPSFGridder(false), itsBoxPSFGridder(false),
        itsUsePreconGridder(false), itsDataPartitionType("time")
    {
      setParameters(ip);
      init(parset);
    }

    /// Copy constructor
    ImageEquation::ImageEquation(const ImageEquation& other)
    {
      operator=(other);
    }

    /// Assignment operator
    ImageEquation& ImageEquation::operator=(const ImageEquation& other)
    {
      if(this!=&other)
      {
        itsIdi=other.itsIdi;
        itsGridder = other.itsGridder;
        itsSphFuncPSFGridder = other.itsSphFuncPSFGridder;
        itsBoxPSFGridder = other.itsBoxPSFGridder;
        itsUsePreconGridder = other.itsUsePreconGridder;
        itsDataPartitionType = other.itsDataPartitionType;
        itsVisUpdateObject = other.itsVisUpdateObject;
      }
      return *this;
    }

    ImageEquation::~ImageEquation()
    {
    }

    void ImageEquation::init(const LOFAR::ParameterSet& parset)
    {
        if ( parset.isDefined("gridder.partitiontype") ) {
            itsDataPartitionType = parset.getString("gridder.partitiontype");
        }
        useAlternativePSF(parset);
    }

    /// @todo if the wiener filter is bypassed this may not be needed at all
    /// @brief define whether to use an alternative gridder for the PSF
    /// and/or the preconditioner function
    void ImageEquation::useAlternativePSF(const LOFAR::ParameterSet& parset)
    {
      ASKAP_LOGGER(logger, ".ImageEquation.useAlternativePSF");
      const bool useGentlePCF = parset.getBool("preconditioner.preservecf", false);
      const bool useBoxPSF = parset.getBool("boxforpsf", false);
      const bool useSphPSF = parset.getBool("sphfuncforpsf", false);

      if (useGentlePCF) {
         itsUsePreconGridder = true;
         ASKAPLOG_INFO_STR(logger,
             "A separate tophat-style gridder will be used to calculate the preconditioner function");
      }

      if (useBoxPSF) {
         itsBoxPSFGridder = true;
         ASKAPLOG_INFO_STR(logger,
             "The box (nearest neighbour) gridder will be used to calculate the PSF");
         if (useSphPSF) {
             ASKAPLOG_WARN_STR(logger,
                 "The spheroidal function will not be used to calculate the PSF");
         }
      } else if (useSphPSF) {
         itsSphFuncPSFGridder = true;
         ASKAPLOG_INFO_STR(logger,
             "The default spheroidal function gridder will be used to calculate the PSF");
      } else {
         ASKAPLOG_INFO_STR(logger,
             "The PSF will be calculated by the same gridder type as used for the model and residuals");
      }
    }

    /// Access the parameters
    const Params& ImageEquation::parameters() const {
      ASKAPDEBUGASSERT(itsParams);
      return *itsParams;
    };

    /// Set the parameters to new values
    /// @param ip Parameters
    void ImageEquation::setParameters(const Params& ip) 
    {
       if (rwParameters()) {
           // use assignment operator of Params class, i.e. 
           // copy will happen at itsParams side and shared pointer will
           // not change (we somewhat rely on this behavior in the calibration
           // code)
           *rwParameters() = ip;
       } else {
         // current parameters are empty, clone the input parameters and setup
         // shared pointer
         itsParams = ip.clone(); 
         // call the virtual method just in case some derived class needs to do a cache update
         rwParameters();
       }
    }

    // FIXME: This will be needed if we ever do distributed Taylor terms in the way they are done in
    //        yandasoft, with different terms on different ranks. However I believe the CDR plan for SKA is
    //        have a single gridder per frequency, and apply the MFS weighting during multiple image
    //        reductions. In that case all Taylor term degriddings operations for a given channel may be done
    //        on the same node.
    // FIXME: Also needed if polarisations are ever distributed?
    /// @brief setup object function to update degridded visibilities
    /// @details For the parallel implementation of the measurement equation we need
    /// inter-rank communication. To avoid introducing cross-dependency of the measurement
    /// equation and the MPI one can use polymorphic object function to sum degridded visibilities
    /// across all required ranks in the distributed case and do nothing otherwise.
    /// By default, this class doesn't alter degridded visibilities.
    /// @param[in] obj new object function (or an empty shared pointer to turn this option off)
    void ImageEquation::setVisUpdateObject(const boost::shared_ptr<askap::synthesis::IVisCubeUpdate> &obj)
    {
        itsVisUpdateObject = obj;
    }

    /// @brief assign a different iterator
    /// @details This is a temporary method to assign a different iterator.
    /// All this business is a bit ugly, but should go away when all
    /// measurement equations are converted to work with accessors.
    /// @param idi shared pointer to a new iterator
    //void ImageEquation::setIterator(accessors::IDataSharedIter& idi)
    //{
    //  itsIdi = idi;
    //}

    // Calculate the residual visibility and image. We transform the model on the fly
    // so that we only have to read (and write) the data once. This uses more memory
    // but cuts down on IO
    void ImageEquation::calcImagingEquations(askap::scimath::ImagingNormalEquations& ne)
    {
        ASKAPTRACE("ImageEquation::calcImagingEquations");
        ASKAP_LOGGER(logger, ".ImageEquation.calcImagingEquations");
       
        // We will need to loop over all completions i.e. all sources
        const std::vector<std::string> completions(parameters().completions("image"));
       
        // To minimize the number of data passes, we keep copies of the gridders in memory, and
        // switch between these. This optimization may not be sufficient in the long run.
        // Set up initial gridders for model and for the residuals. This enables us to
        // do both at the same time.
       
        for (std::vector<std::string>::const_iterator it=completions.begin();it!=completions.end();it++)
        {
            const string imageName("image"+(*it));
            askap::synthesis::SynthesisParamsHelper::clipImage(parameters(),imageName);
           
            if(itsModelGridders.count(imageName)==0) {
                itsModelGridders[imageName]=itsGridder->clone();
            }
            if(itsResidualGridders.count(imageName)==0) {
                itsResidualGridders[imageName]=itsGridder->clone();
            }
            if(itsPSFGridders.count(imageName)==0) {
                if (itsBoxPSFGridder) {
                   boost::shared_ptr<BoxVisGridder> psfGridder(new BoxVisGridder);
                   itsPSFGridders[imageName] = psfGridder;
                } else if (itsSphFuncPSFGridder) {
                   boost::shared_ptr<SphFuncVisGridder> psfGridder(new SphFuncVisGridder);
                   itsPSFGridders[imageName] = psfGridder;
                } else {
                   itsPSFGridders[imageName] = itsGridder->clone();
                }
            }
            if(itsUsePreconGridder && itsPreconGridders.count(imageName)==0) {
                // preconditioning of higher order terms is set from term 0
                bool isMFS = (imageName.find(".taylor.") != std::string::npos);
                bool isTT0 = (imageName.find(".taylor.0") != std::string::npos);
                if (isTT0 || !isMFS) {
                  // Should this be a clone of the psf or the image?
                  //itsPreconGridders[imageName] = itsGridder->clone();
                  itsPreconGridders[imageName] = itsPSFGridders[imageName]->clone();
                }
            }
            if (itsCoordSystems.count(imageName) == 0) {
                itsCoordSystems[imageName] =
                    askap::synthesis::SynthesisParamsHelper::coordinateSystem(parameters(),imageName);
            }
        }
        // Now we initialise appropriately
        ASKAPLOG_DEBUG_STR(logger, "Initialising for model degridding and residual gridding" );
        if (completions.size() == 0) {
            ASKAPLOG_WARN_STR(logger,
                "Found no free image parameters, this rank will not contribute usefully to normal equations");
        }
       
        // if data partitions are unset, interate through the data and create them
        if (itsVisData == nullptr) {
            ASKAPLOG_DEBUG_STR(logger, "Initialising a new VisData container" );
            itsVisData = partitionData();
        }
        else {
            ASKAPLOG_DEBUG_STR(logger, "Using existing VisData container" );
        }
      
        // @TODO incorporate this into partitionData()?
        const casacore::Vector<casacore::Stokes::StokesTypes> imagePols = itsVisData->getImagePols();
        for (uint k = 0; k<itsVisData->nPartitions(); ++k) {
            itsVisData->visInfo(k)->convertPol(imagePols);
        }

        bool somethingHasToBeDegridded = false;
        // FIXME: Yandasoft can output multiple images, but that doesn't really work with cached gridding data.
        //        Remove support for that from calcImagingEquations and add an exception if completions.size()!=1. 
       for (std::vector<std::string>::const_iterator it=completions.begin();it!=completions.end();it++)
        {
            string imageName("image"+(*it));
            ASKAPLOG_DEBUG_STR(logger, "Initialising for " << imageName);
            const Axes axes(parameters().axes(imageName));
            casacore::Array<imtype> imagePixels;
            #ifdef ASKAP_FLOAT_IMAGE_PARAMS
                imagePixels = parameters().valueF(imageName);
            #else
                imagePixels = parameters().valueT(imageName);
            #endif
            const casacore::IPosition imageShape(imagePixels.shape());
            /// First the model
            itsModelGridders[imageName]->customiseForContext(*it);
            itsModelGridders[imageName]->initialiseCommon(itsVisData);
            itsModelGridders[imageName]->initialiseDegrid(itsVisData, imagePixels);
            if (!itsModelGridders[imageName]->isModelEmpty()) {
                somethingHasToBeDegridded = true;
            }
            /// Now the residual images, dopsf=false, dopcf=false
            itsResidualGridders[imageName]->customiseForContext(*it);
            itsResidualGridders[imageName]->initialiseCommon(itsVisData);
            itsResidualGridders[imageName]->initialiseGrid(itsVisData, false);
            // and PSF gridders, dopsf=true, dopcf=false
            itsPSFGridders[imageName]->customiseForContext(*it);
            itsPSFGridders[imageName]->initialiseCommon(itsVisData);
            itsPSFGridders[imageName]->initialiseGrid(itsVisData, true);
            // and PCF gridders, dopsf=false, dopcf=true
            if (itsUsePreconGridder && (itsPreconGridders.count(imageName)>0)) {
                itsPreconGridders[imageName]->customiseForContext(*it);
                itsPreconGridders[imageName]->initialiseCommon(itsVisData);
                itsPreconGridders[imageName]->initialiseGrid(itsVisData, false, true);
            }
        }
        // synchronise emtpy flag across multiple ranks if necessary
        if (itsVisUpdateObject) {
            itsVisUpdateObject->aggregateFlag(somethingHasToBeDegridded);
        }

        // Now we loop through all the data
        if (somethingHasToBeDegridded) {
            ASKAPLOG_DEBUG_STR(logger, "Degridding model and gridding residuals" );
        } else {
            ASKAPLOG_DEBUG_STR(logger, "Gridding residuals" );
        }
        size_t counterGrid = 0, counterDegrid = 0;
        for (uint k = 0; k<itsVisData->nPartitions(); ++k) {
            // Accumulate model visibility for all models
            if (somethingHasToBeDegridded) {
                for (std::vector<std::string>::const_iterator it=completions.begin();it!=completions.end();++it) {
                     const std::string imageName("image"+(*it));
                     if (!itsModelGridders[imageName]->isModelEmpty()) {
                         itsModelGridders[imageName]->degrid(itsVisData->griddingInfo(k));
                         counterDegrid+=itsVisData->griddingInfo(k)->nSample();
                     }
                }
                // optional aggregation of visibilities in the case of distributed MFS model
                // somethingHasToBeDegridded is supposed to have consistent value across all participating ranks
                if (itsVisUpdateObject) {
                    /// @todo FIXME will need to modify this for the new data type. Not currently used.
                    //itsVisUpdateObject->update(accBuffer.rwVisibility());
                }
                itsVisData->griddingInfo(k)->subtractModel();
            }
            /// Now we can calculate the residual visibility and image
            size_t tempCounter = 0;
            for (size_t i = 0; i<completions.size(); ++i) {
                const string imageName("image"+completions[i]);
                if (parameters().isFree(imageName)) {
                    itsResidualGridders[imageName]->grid(itsVisData->griddingInfo(k));
                    itsPSFGridders[imageName]->grid(itsVisData->griddingInfo(k));
                    if (itsUsePreconGridder && (itsPreconGridders.count(imageName)>0)) {
                        itsPreconGridders[imageName]->grid(itsVisData->griddingInfo(k));
                    }
                    tempCounter += itsVisData->griddingInfo(k)->nSample();
                }
            }
            counterGrid += tempCounter;
        }
        if (somethingHasToBeDegridded) {
            ASKAPLOG_DEBUG_STR(logger, "Finished degridding model and gridding residuals" );
        } else {
            ASKAPLOG_DEBUG_STR(logger, "Finished gridding residuals" );
        }
        ASKAPLOG_DEBUG_STR(logger, "Number of vis samples iterated through is "<<counterGrid<<
                                   " (gridding) and "<<counterDegrid<<" (degridding)");

        // We have looped over all the data, so now we have to complete the
        // transforms and fill in the normal equations with the results from the
        // residual gridders
        if (itsUsePreconGridder) {
            ASKAPLOG_DEBUG_STR(logger,
                "Adding residual image, PSF, preconditioner function and weights image to the normal equations" );
        } else {
            ASKAPLOG_DEBUG_STR(logger,
                "Adding residual image, PSF and weights image to the normal equations" );
        }
        for (std::vector<std::string>::const_iterator it=completions.begin();it!=completions.end();++it)
        {
            const string imageName("image"+(*it));
            const casacore::IPosition imageShape(parameters().shape(imageName));
            ASKAPLOG_INFO_STR(logger,"Name: " << imageName << " Shape: " << imageShape);
            casacore::Array<imtype> imageDeriv(imageShape);
            casacore::Array<imtype> imagePSF(imageShape);
            casacore::Array<imtype> imageWeight(imageShape);
            itsResidualGridders[imageName]->finaliseGrid(imageDeriv);
           
            // for debugging/research, store grid prior to FFT
            // boost::shared_ptr<VisGridder>
            //     tvg = boost::dynamic_pointer_cast<VisGridder>(itsResidualGridders[imageName]);
            // if (tvg) {
            //    tvg->storeGrid("uvcoverage"+(*it),0);
            //}
            // end debugging code
           
            itsPSFGridders[imageName]->finaliseGrid(imagePSF);
            itsResidualGridders[imageName]->finaliseWeights(imageWeight);
           
            // just to deallocate the grid memory
            itsModelGridders[imageName]->finaliseDegrid();
           
            casacore::IPosition vecShape(1, imagePSF.nelements());
           
            casacore::Vector<imtype> imagePreconVec;
            if (itsUsePreconGridder && (itsPreconGridders.count(imageName)>0)) {
                casacore::Array<imtype> imagePrecon(imageShape);
                itsPreconGridders[imageName]->finaliseGrid(imagePrecon);
                imagePreconVec.reference(imagePrecon.reform(vecShape));
            }
           
            {
                casacore::IPosition reference(4, imageShape(0)/2, imageShape(1)/2, 0, 0);
                casacore::Vector<imtype> imagePSFVec(imagePSF.reform(vecShape));
                casacore::Vector<imtype> imageWeightVec(imageWeight.reform(vecShape));
                casacore::Vector<imtype> imageDerivVec(imageDeriv.reform(vecShape));
                ne.addSlice(imageName, imagePSFVec, imageWeightVec, imagePreconVec,
                    imageDerivVec, imageShape, reference,itsCoordSystems[imageName]);
            }
        }
    }

    VisData::ShPtr ImageEquation::partitionData()
    {
        ASKAPTRACE("ImageEquation::partitionData");
        ASKAP_LOGGER(logger, ".ImageEquation.partitionData");
        // Need to include metadata about the output images, since phasing happens before gridding
        // FIXME: Yandasoft can output multiple images, but that doesn't really work with cached gridding data.
        //        Remove support for that from calcImagingEquations and add an exception if completions.size()!=1. 
        const std::vector<std::string> completions(parameters().completions("image"));
        const string imageName("image"+completions[0]);
        const scimath::Axes axes(parameters().axes(imageName));
        const casacore::IPosition shape(parameters().shape(imageName));

        return VisData::ShPtr(new VisData(itsIdi,axes,shape,itsDataPartitionType));
    }

    /// @brief Initialise VisData and gridder for a data partition
    /// @param visInfo visInfo object
    /// @return shared pointer to the VisData object
    /// @note done in ImageEquation to access parameters() metadata
    VisData::ShPtr ImageEquation::initImagingEquations(boost::shared_ptr<VisInfo> &visInfo)
    {
        ASKAPTRACE("ImageEquation::initVisData");
        ASKAP_LOGGER(logger, ".ImageEquation.initVisData");
        // Need to include metadata about the output images, since phasing happens before gridding
        // FIXME: Yandasoft can output multiple images, but that doesn't really work with cached gridding data.
        //        Remove support for that from calcImagingEquations and add an exception if completions.size()!=1. 
        const std::vector<std::string> completions(parameters().completions("image"));
        const string imageName("image"+completions[0]);
        const scimath::Axes axes(parameters().axes(imageName));
        const casacore::IPosition shape(parameters().shape(imageName));

        VisData::ShPtr visData(new VisData(visInfo,axes,shape));

        itsGridder->initialiseCommon(visData);

        // DAM should we just set itsVisData and not return this? I think so
        return visData;
    }

    /// @brief Initialise VisData and gridder for a data partition
    /// @param griddingInfo griddingInfo object
    /// @return shared pointer to the VisData object
    /// @note done in ImageEquation to access parameters() metadata
    VisData::ShPtr ImageEquation::initImagingEquations(boost::shared_ptr<GriddingInfo> &griddingInfo)
    {
        ASKAPTRACE("ImageEquation::initVisData");
        ASKAP_LOGGER(logger, ".ImageEquation.initVisData");
        // Need to include metadata about the output images, since phasing happens before gridding
        // FIXME: Yandasoft can output multiple images, but that doesn't really work with cached gridding data.
        //        Remove support for that from calcImagingEquations and add an exception if completions.size()!=1. 
        const std::vector<std::string> completions(parameters().completions("image"));
        const string imageName("image"+completions[0]);
        const scimath::Axes axes(parameters().axes(imageName));
        const casacore::IPosition shape(parameters().shape(imageName));

        VisData::ShPtr visData(new VisData(griddingInfo,axes,shape));

        return visData;
    }

    /// @brief Invert part of the normal equation calc: predict -> subtract -> invert, one row of vis at a time
    /// @details Grid and image the residual visibilities.
    /// @param ne Normal equations
    /// @param visData visData object containing griddingInfo
    void ImageEquation::calcImagingEquationsInvert(askap::scimath::ImagingNormalEquations& ne,
                                                   const VisData::ShPtr &visData)
    {
        ASKAPTRACE("ImageEquation::calcImagingEquationsInvert");
        ASKAP_LOGGER(logger, ".ImageEquation.calcImagingEquationsInvert");
       
        // FIXME: Yandasoft can output multiple images, but that doesn't really work with cached gridding data.
        //        Remove support for that from calcImagingEquations and add an exception if completions.size()!=1.
        // DAM but what abould Taylor terms? Are they looped over?
        // DAM should we just bite to bullet and move TT support to the merge?
        // We will need to loop over all completions i.e. all sources
        const std::vector<std::string> completions(parameters().completions("image"));
        if (completions.size() == 0) {
            // DAM I think we need to throw an exception here (the following lines will fail). What is the use case?
            ASKAPLOG_WARN_STR(logger,
                "Found no free image parameters, this rank will not contribute usefully to normal equations");
        }
        const string imageName("image"+completions[0]);
       
        // To minimize the number of data passes, we keep copies of the gridders in memory, and
        // switch between these. This optimization may not be sufficient in the long run.
        // Set up initial gridders for model and for the residuals. This enables us to
        // do both at the same time.
       
        askap::synthesis::SynthesisParamsHelper::clipImage(parameters(),imageName);
        
        if(itsResidualGridders.count(imageName)==0) {
            itsResidualGridders[imageName]=itsGridder->clone();
        }
        if(itsPSFGridders.count(imageName)==0) {
            if (itsBoxPSFGridder) {
               boost::shared_ptr<BoxVisGridder> psfGridder(new BoxVisGridder);
               itsPSFGridders[imageName] = psfGridder;
            } else if (itsSphFuncPSFGridder) {
               boost::shared_ptr<SphFuncVisGridder> psfGridder(new SphFuncVisGridder);
               itsPSFGridders[imageName] = psfGridder;
            } else {
               itsPSFGridders[imageName] = itsGridder->clone();
            }
        }
        if(itsUsePreconGridder && itsPreconGridders.count(imageName)==0) {
            // preconditioning of higher order terms is set from term 0
            bool isMFS = (imageName.find(".taylor.") != std::string::npos);
            bool isTT0 = (imageName.find(".taylor.0") != std::string::npos);
            if (isTT0 || !isMFS) {
              // Should this be a clone of the psf or the image?
              //itsPreconGridders[imageName] = itsGridder->clone();
              itsPreconGridders[imageName] = itsPSFGridders[imageName]->clone();
            }
        }
        if (itsCoordSystems.count(imageName) == 0) {
            itsCoordSystems[imageName] =
                askap::synthesis::SynthesisParamsHelper::coordinateSystem(parameters(),imageName);
        }

        // Now we initialise appropriately
        ASKAPLOG_DEBUG_STR(logger, "Initialising gridding for " << imageName);
      
        // shortcuts to grid data
        const Axes axes(parameters().axes(imageName));
        casacore::Array<imtype> imagePixels;
        #ifdef ASKAP_FLOAT_IMAGE_PARAMS
            imagePixels = parameters().valueF(imageName);
        #else
            imagePixels = parameters().valueT(imageName);
        #endif
        const casacore::IPosition imageShape(imagePixels.shape());
        ASKAPASSERT(imageShape.isEqual(parameters().shape(imageName)));

        // residual gridders, dopsf=false, dopcf=false
        // DAM commenting these out will break TTs, so store in visData? Have already removed the loops though...
        //itsResidualGridders[imageName]->customiseForContext(*it);
        itsResidualGridders[imageName]->initialiseGrid(visData, false);

        // PSF gridders, dopsf=true, dopcf=false
        //itsPSFGridders[imageName]->customiseForContext(*it);
        itsPSFGridders[imageName]->initialiseGrid(visData, true);
        // PCF gridders, dopsf=false, dopcf=true
        if (itsUsePreconGridder && (itsPreconGridders.count(imageName)>0)) {
            //itsPreconGridders[imageName]->customiseForContext(*it);
            itsPreconGridders[imageName]->initialiseGrid(visData, false, true);
        }

        // loop through and grid all the data

        ASKAPLOG_INFO_STR(logger,"Gridding residuals and PSF for " << imageName << " with shape " << imageShape);

        size_t counterGrid = 0;
        for (uint k = 0; k<visData->nPartitions(); ++k) {
            /// Calculate the residual visibility and image
            if (parameters().isFree(imageName)) {
                itsResidualGridders[imageName]->grid(visData->griddingInfo(k));
                itsPSFGridders[imageName]->grid(visData->griddingInfo(k));
                if (itsUsePreconGridder && (itsPreconGridders.count(imageName)>0)) {
                    itsPreconGridders[imageName]->grid(visData->griddingInfo(k));
                }
                counterGrid += visData->griddingInfo(k)->nSample();
            }
        }
        ASKAPLOG_DEBUG_STR(logger, "Finished gridding residuals" );
        ASKAPLOG_DEBUG_STR(logger, "Number of vis samples iterated through is "<<counterGrid);

        // We have looped over all the data, so now we have to complete the transforms
        // and fill in the normal equations with the results from the residual gridders
        if (itsUsePreconGridder) {
            ASKAPLOG_DEBUG_STR(logger,
                "Finalising residual, PSF, preconditioner and weights images and adding to the normal equations" );
        } else {
            ASKAPLOG_DEBUG_STR(logger,
                "Finalising residual, PSF and weights images and adding to the normal equations" );
        }

        casacore::Array<imtype> imageDeriv(imageShape);
        casacore::Array<imtype> imagePSF(imageShape);
        casacore::Array<imtype> imageWeight(imageShape);
        itsResidualGridders[imageName]->finaliseGrid(imageDeriv);
        
        itsPSFGridders[imageName]->finaliseGrid(imagePSF);
        itsResidualGridders[imageName]->finaliseWeights(imageWeight);
                 
        casacore::IPosition vecShape(1, imagePSF.nelements());
        
        casacore::Vector<imtype> imagePreconVec;
        if (itsUsePreconGridder && (itsPreconGridders.count(imageName)>0)) {
            casacore::Array<imtype> imagePrecon(imageShape);
            itsPreconGridders[imageName]->finaliseGrid(imagePrecon);
            imagePreconVec.reference(imagePrecon.reform(vecShape));
        }
        
        {
            casacore::IPosition reference(4, imageShape(0)/2, imageShape(1)/2, 0, 0);
            casacore::Vector<imtype> imagePSFVec(imagePSF.reform(vecShape));
            casacore::Vector<imtype> imageWeightVec(imageWeight.reform(vecShape));
            casacore::Vector<imtype> imageDerivVec(imageDeriv.reform(vecShape));
            ne.addSlice(imageName, imagePSFVec, imageWeightVec, imagePreconVec,
                imageDerivVec, imageShape, reference,itsCoordSystems[imageName]);
        }

    }

    /// @brief Predict part of the normal equation calc: predict -> subtract -> invert, one row of vis at a time
    /// @details Transform and degrid the model visibilities.
    /// @param ne Normal equations
    /// @param visData visData object containing griddingInfo
    void ImageEquation::calcImagingEquationsPredict(askap::scimath::ImagingNormalEquations& ne,
                                                    VisData::ShPtr &visData)
    {
        ASKAPTRACE("ImageEquation::calcImagingEquationsPredict");
        ASKAP_LOGGER(logger, ".ImageEquation.calcImagingEquationsPredict");
       
        bool somethingHasToBeDegridded = false;

        // FIXME: Yandasoft can output multiple images, but that doesn't really work with cached gridding data.
        //        Remove support for that from calcImagingEquations and add an exception if completions.size()!=1. 
        // We will need to loop over all completions i.e. all sources
        const std::vector<std::string> completions(parameters().completions("image"));
        if (completions.size() == 0) {
            // DAM I think we need to throw an exception here (the following lines will fail). What is the use case?
            ASKAPLOG_WARN_STR(logger,
                "Found no free image parameters, this rank will not contribute usefully to normal equations");
        }
        const string imageName("image"+completions[0]);
       
        // To minimize the number of data passes, we keep copies of the gridders in memory, and
        // switch between these. This optimization may not be sufficient in the long run.
        // Set up initial gridders for model and for the residuals. This enables us to
        // do both at the same time.
       
        askap::synthesis::SynthesisParamsHelper::clipImage(parameters(),imageName);
        
        if(itsModelGridders.count(imageName)==0) {
            itsModelGridders[imageName]=itsGridder->clone();
        }
        if (itsCoordSystems.count(imageName) == 0) {
            itsCoordSystems[imageName] =
                askap::synthesis::SynthesisParamsHelper::coordinateSystem(parameters(),imageName);
        }

        // Now we initialise appropriately
        ASKAPLOG_DEBUG_STR(logger, "Initialising degridding for " << imageName);
      
        // shortcuts to grid data
        const Axes axes(parameters().axes(imageName));
        casacore::Array<imtype> imagePixels;
        #ifdef ASKAP_FLOAT_IMAGE_PARAMS
            imagePixels = parameters().valueF(imageName);
        #else
            imagePixels = parameters().valueT(imageName);
        #endif
        const casacore::IPosition imageShape(imagePixels.shape());
        ASKAPASSERT(imageShape.isEqual(parameters().shape(imageName)));

        // model gridders
        // DAM commenting this out will break TTs, so store in visData? Have already removed the loops though...
        //itsModelGridders[imageName]->customiseForContext(*it);
        itsModelGridders[imageName]->initialiseDegrid(visData, imagePixels);
        if (!itsModelGridders[imageName]->isModelEmpty()) {
            somethingHasToBeDegridded = true;
        }

        // synchronise emtpy flag across multiple ranks if necessary
        if (itsVisUpdateObject) {
            itsVisUpdateObject->aggregateFlag(somethingHasToBeDegridded);
        }

        // loop through and degrid all the data

        ASKAPLOG_INFO_STR(logger,"Degridding model for " << imageName << " with shape " << imageShape);
        if (!somethingHasToBeDegridded) {
            ASKAPLOG_DEBUG_STR(logger, "There is nothing to degrid!" );
        }

        size_t counterDegrid = 0;
        if (somethingHasToBeDegridded) {
            for (uint k = 0; k<visData->nPartitions(); ++k) {
                // Accumulate model visibility for all models
                itsModelGridders[imageName]->degrid(visData->griddingInfo(k));
                counterDegrid += visData->griddingInfo(k)->nSample();
                // optional aggregation of visibilities in the case of distributed MFS model
                // somethingHasToBeDegridded is supposed to have consistent value across all participating ranks
                if (itsVisUpdateObject) {
                    /// @todo FIXME will need to modify this for the new data type. Not currently used.
                    //itsVisUpdateObject->update(accBuffer.rwVisibility());
                }

                visData->griddingInfo(k)->subtractModel();

            }
            ASKAPLOG_DEBUG_STR(logger, "Finished degridding model and gridding residuals" );
        }
        ASKAPLOG_DEBUG_STR(logger, "Number of vis samples iterated through is "<<counterDegrid);

        ASKAPLOG_DEBUG_STR(logger, "Finalising model degridding");

        // deallocate the grid memory
        itsModelGridders[imageName]->finaliseDegrid();

    }

    /// @brief shared pointer to paramters
    /// @details Due to caching, derived classes may need to know when
    /// the parameters of the equation have been updated. To track all
    /// updates, itsParams is made private. All changes to parameters are
    /// done via this method (including setParameters exposed to the user).
    /// @note This method allows non-const manipulation of the parameters, but
    /// not the change of the actual shared pointer class. The latter is done only
    /// inside this class and not in derived classes.
    /// @return non-const reference to the shared pointer
    const Params::ShPtr& ImageEquation::rwParameters() const throw() 
    {
        return itsParams;
    }

    /// @brief reference the given parameter object
    /// @details Sometimes it is handy to have a number of equations sharing exactly
    /// the same parameters and use reference semantics. A call to this method allows
    /// to reference itsParams member of this class to any given shared pointer to
    /// scimath::Params. In particular, a shared pointer can be obtained using rwParameters
    /// method. Use with caution. There is some legacy code/design which keep track of 
    /// changes in parameters by overriding rwParameters. This methanism will not work 
    /// correctly if referencing is used. Ideally, we want to convert this approach to
    /// use change monitors together with scimath::Params class.
    /// @param[in] params shared pointer to scimath::Params class to adopt in this equation
    void ImageEquation::reference(const Params::ShPtr &params)
    {
        itsParams = params;
        // call the virtual method just in case some derived class needs to do a cache update
        rwParameters();       
    }

}
