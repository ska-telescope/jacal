/// @file GenerateGridData.h
///
/// @brief Calculate imaging Normal Equations (vis predict followed by invert)
/// @details This class encorporates all of the tasks needed to form imaging
/// Normal Equations: read from a measurement set; degrid model visibilities;
/// subtract model visibilities; grid residual visibilities and FFT the grid
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_GENERATEGRIDDATA_H
#define ASKAP_FACTORY_GENERATEGRIDDATA_H

#include "rename.h"
#include <daliuge/DaliugeApplication.h>

#include <factory/gridders/VisData.h>

#include <casacore/casa/Quanta/MVDirection.h>

#include <boost/shared_ptr.hpp>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>

// Yandasoft includes
#include <scimath/fitting/Params.h>

namespace askap {

    /*!
    * \brief GenerateGridData
    * \brief Calculates the Normal Equations
    * \details This class encorporates all of the tasks needed to form imaging
    *  Normal Equations: read from a measurement set; degrid model visibilities;
    *  subtract model visibilities; grid residual visibilities and FFT the grid
    * \par EAGLE_START
    * \param category DynlibApp
    * \param[in] param/libpath Library Path/"%JACAL_SO%"/String/readonly/
    *     \~English The path to the JACAL librarc
    * \param[in] param/Arg01 Arg01/name=GenerateGridData/String/readonly/
    *     \~English
    * \param[in] port/Config Config/LOFAR::ParameterSet/
    *     \~English ParameterSet descriptor for the image solver
    * \param[in] port/VisInfo VisInfo/VisInfo/
    *     \~English Visiblity metadata
    * \param[out] port/GriddingInfo GriddingInfo/GriddingInfo/
    *     \~English Gridding metadata
    * \param[out] port/vis vis/std::vector<std::vector<std::complex<float> > >/
    *     \~English Visibility data
    * \par EAGLE_END
    */
    class GenerateGridData : public DaliugeApplication
    {
    public:

        typedef boost::shared_ptr<GenerateGridData> ShPtr;

        GenerateGridData(dlg_app_info *raw_app);

        static inline std::string ApplicationName() { return "GenerateGridData";}

        virtual ~GenerateGridData();

        static DaliugeApplication::ShPtr createDaliugeApplication(dlg_app_info *raw_app);

        virtual int init(const char ***arguments);

        virtual int run();

        virtual void data_written(const char *uid, const char *data, size_t n);

        virtual void drop_completed(const char *uid, drop_status status);


        private:

            //! @brief Parameter set
            //! @details key value list of configuration options
            LOFAR::ParameterSet itsParset;

            // Its tangent point
            std::vector<casacore::MVDirection> itsTangent; ///< the tangent point of the current grid

            int itsRank;

    };

} // namespace askap


#endif //

