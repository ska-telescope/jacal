/// @file ReduceNE.h
///
/// @brief Merge two Normal Equation objects
/// @details Use askap::scimath::ImagingNormalEquations::merge() to merge two
/// NEs. This may be a simple summation of pixels or a weighted summation, and
/// may use the askap linmos task to merge onto a larger mosaic.
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

#ifndef ASKAP_FACTORY_REDUCENE_H
#define ASKAP_FACTORY_REDUCENE_H

#include "rename.h"
#include <daliuge/DaliugeApplication.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>

namespace askap {
    /*!
    * \brief ReduceNE
    * \brief Merge two Normal Equation objects
    * \details Use askap::scimath::ImagingNormalEquations::merge() to merge two NEs
    * \par EAGLE_START
    * \param category DynlibApp
    * \param[in] param/libpath Library Path/"%JACAL_SO%"/String/readonly/
    *     \~English The path to the JACAL library
    * \param[in] param/Arg01 Arg01/name=ReduceNE/String/readonly/
    *     \~English
    * \param[in] port/Config Config/LOFAR::ParameterSet/
    *     \~English The Config file
    *     \~Chinese 配置文件
    * \param[in] port/Normal Normal/scimath::ImagingNormalEquations/
    *     \~English ImagingNormalEquations to merge
    * \param[in] port/Normal Normal/scimath::ImagingNormalEquations/
    *     \~English ImagingNormalEquations to merge
    * \param[out] port/Normal Normal/scimath::ImagingNormalEquations/
    *     \~English Merged ImagingNormalEquations to merged further or solve
    * \par EAGLE_END
    */
    class ReduceNE : public DaliugeApplication
    {
    public:

        ReduceNE(dlg_app_info *raw_app);

        static inline std::string ApplicationName() { return "ReduceNE";}

        virtual ~ReduceNE();

        static DaliugeApplication::ShPtr createDaliugeApplication(dlg_app_info *raw_app);

        virtual int init(const char ***arguments);

        virtual int run();

        virtual void data_written(const char *uid, const char *data, size_t n);

        virtual void drop_completed(const char *uid, drop_status status);

    };

} // namespace askap


#endif //
