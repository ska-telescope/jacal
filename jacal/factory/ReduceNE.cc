/// @file ReduceNE.cc
///
/// @brief Merge two Normal Equation objects
/// @details Use askap::scimath::ImagingNormalEquations::merge() to merge two
/// NEs. This may be a simple summation of pixels or a weighted summation, and
/// may use the askap linmos task to merge onto a larger mosaic.
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

/// for logging
#define ASKAP_PACKAGE_NAME "ReduceNE"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_ReduceNE() {
        return std::string("ReduceNE; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_ReduceNE()

#include <vector>
#include <mutex>

// Local includes
#include <daliuge/DaliugeApplication.h>
#include <factory/ReduceNE.h>
#include <factory/NEUtils.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
#include <askap/StatReporter.h>

#include <imagemath/linmos/LinmosAccumulator.h>

#include <string.h>
#include <sys/time.h>

namespace askap {

    ReduceNE::ReduceNE(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
    }

    ReduceNE::~ReduceNE() {
    }

    DaliugeApplication::ShPtr ReduceNE::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return ReduceNE::ShPtr(new ReduceNE(raw_app));
    }

    int ReduceNE::init(const char ***arguments) {

        while (1) {

            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }

            arguments++;
        }

        return 0;
    }

    int ReduceNE::run() {

//#ifndef ASKAP_PATCHED
//        static std::mutex safety;
//#endif

        ASKAP_LOGGER(logger, ".ReduceNE.run");

        askap::StatReporter stats;

        // Lets get the key-value-parset
        char buf[64*1024];

        askap::scimath::ImagingNormalEquations::ShPtr
            ne1 = askap::scimath::ImagingNormalEquations::ShPtr(new askap::scimath::ImagingNormalEquations());
        askap::scimath::ImagingNormalEquations::ShPtr
            ne2 = askap::scimath::ImagingNormalEquations::ShPtr(new askap::scimath::ImagingNormalEquations());

        if (has_input("Normal",0)) {
            NEUtils::receiveNE(ne1, input("Normal",0));
        } else {
            ASKAPLOG_WARN_STR(logger, "No input for the first Normal Equation object");
            return -1;
        }

        if (has_input("Normal",1)) {
            NEUtils::receiveNE(ne2, input("Normal",1));
        } else {
            ASKAPLOG_WARN_STR(logger, "No input for the second Normal Equation object");
            return -1;
        }

        // TODO: check if they have the same name but different coordinate systems, and only then set up for linmos
        // ASKAPCHECK(coordinatesAreConsistent(csys1, csys2)

        ne1->weightType(0);     // askap::imagemath::FROM_WEIGHT_IMAGES
        ne1->weightState(2);    // askap::imagemath::WEIGHTED
        ne2->weightType(0);
        ne2->weightState(2);

        ASKAPLOG_INFO_STR(logger, "Merging Normal Equations");
        ne1->merge(*ne2);

//#ifndef ASKAP_PATCHED
//        std::lock_guard<std::mutex> guard(safety);
//#endif

        // Send the resulting normal equation images to the output port
        NEUtils::sendNE(ne1, output("Normal"));

        stats.logSummary();

        return 0;
    }

    void ReduceNE::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void ReduceNE::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }

} // namespace
