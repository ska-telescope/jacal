/// @file GridInvert.cc
///
/// @brief Calculate model visibilities (vis predict)
/// @details This class forms a set of model visibilties by degridding a model
/// image.
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "GridInvert"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_GridInvert() {
        return std::string("GridInvert; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_GridInvert()

#include <vector>
#include <mutex>

// Local includes
#include <daliuge/DaliugeApplication.h>
#include <factory/GridInvert.h>
#include <factory/NEUtils.h>

// Yandasoft Data Source and Iterator classes (common to all data access methods that use Yandasoft DataAccessors)
#include <dataaccess/TableDataSource.h>
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>
#include <dataaccess/IDataConverter.h>
#include <dataaccess/IDataSelector.h>

// Yandasoft DataAccessors, DataIterators and Gridders
#include <measurementequation/ImageFFTEquation.h>
#include <gridding/IVisGridder.h>
#include <gridding/VisGridderFactory.h>

// Yandasoft DataAccessors with local GriddingInfo and Gridders
#include <factory/ImageEquation.h>
#include <factory/gridders/IVisGridder.h>
#include <factory/gridders/VisGridderFactory.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
#include <askap/StatReporter.h>

ASKAP_LOGGER(logger, ".GridInvert");

// params helpers
#include <measurementequation/SynthesisParamsHelper.h>
#include <measurementequation/ImageParamsHelper.h>
#include <parallel/GroupVisAggregator.h>

#include <scimath/fitting/Params.h>

#include <string.h>
#include <sys/time.h>

namespace askap {

    GridInvert::GridInvert(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
        this->itsModel.reset(new scimath::Params(true));
        this->itsRank = NEUtils::getRank(raw_app->uid);
        if (this->itsRank < 0) {
            this->itsRank = 0;
        }
    }

    GridInvert::~GridInvert() {
    }

    DaliugeApplication::ShPtr GridInvert::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return GridInvert::ShPtr(new GridInvert(raw_app));
    }

    int GridInvert::init(const char ***arguments) {
        ASKAP_LOGGER(logger, ".GridInvert.init");

        while (1) {

            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }

            arguments++;
        }

        return 0;
    }

    int GridInvert::run() {

#ifndef ASKAP_PATCHED
        static std::mutex safety;
#endif

        ASKAP_LOGGER(logger, ".GridInvert.run");

        askap::StatReporter stats;

        // Lets get the key-value-parset
        char buf[64*1024];

        if (has_input("Config")) {
            size_t n_read = input("Config").read(buf, 64*1024);
            if (n_read == 0) {
                ASKAPLOG_WARN_STR(logger, "Nothing read from input Config");
                return -1;
            }
        } else {
            ASKAPLOG_WARN_STR(logger, "No input Config");
            return -1;
        }

        LOFAR::ParameterSet parset(true);
        parset.adoptBuffer(buf);

        // we need to fill the local parset with parameters that maybe missing
        try {
#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif
            NEUtils::finaliseParameters(parset, this->itsRank);
        }
        catch (std::runtime_error)
        {
            return -1;
        }
        this->itsParset = parset.makeSubset("Cimager.");

        {

#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif
            ASKAPLOG_INFO_STR(logger, "Initializing the model images");

            // Create the specified images from the definition in the
            // parameter set. We can solve for any number of images
            // at once (but you may/will run out of memory!)

            askap::synthesis::SynthesisParamsHelper::setUpImages(itsModel, this->itsParset.makeSubset("Images."));
        }

        ASKAPLOG_INFO_STR(logger, "Current model held by the drop: "<<*itsModel);

        {
#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif

            // Receive VisInfo from the input port
            ASKAPCHECK(has_input("GriddingInfo"), "GridInvert requires GriddingInfo input port");
            boost::shared_ptr<GriddingInfo> griddingInfo(new GriddingInfo());
            NEUtils::receiveGriddingInfo(griddingInfo, input("GriddingInfo"));
            ASKAPLOG_INFO_STR(logger, "Receiving and unpacking griddingInfo with "<<
                                      griddingInfo->nSample()<<" samples, "<<
                                      griddingInfo->nPol()<<" polarisations and "<<
                                      griddingInfo->nChannel()<<" channels");

            // Receive vis from the input port
            ASKAPCHECK(has_input("vis"), "GridInvert requires vis input port");
            std::vector<std::vector<std::complex<float> > > vis;
            NEUtils::receiveVis(vis, griddingInfo->nSample(), griddingInfo->nPol(), input("vis"));
            ASKAPLOG_INFO_STR(logger, "Receiving and unpacking vis with "<<
                                      vis.size()<<" samples and "<< vis[0].size()<<" polarisations");

            // Update vis in griddingInfo
            // Could just do this in NEUtils::receiveVis, but this seems more general
            griddingInfo->updateVis(vis);

            // initialise a normal equation
            askap::scimath::ImagingNormalEquations::ShPtr ne;
            ne = askap::scimath::ImagingNormalEquations::ShPtr(new askap::scimath::ImagingNormalEquations(*itsModel));

            const string dataAccess = this->itsParset.getString("gridder.dataaccess", "datapartitions");
            ASKAPCHECK(dataAccess == "datapartitions", "Only dataaccess=datapartitions is possible in Ingest");

            // build a gridder
            IVisGridder::ShPtr gridder = VisGridderFactory::make(this->itsParset);

            // Define an imaging equation with an empty model (or pick one up from the parset)
            ASKAPLOG_INFO_STR(logger,"Building FFT/measurement equation");
            boost::shared_ptr<ImageEquation> imgEq(new ImageEquation (*itsModel, gridder, itsParset));
            ASKAPDEBUGASSERT(imgEq);

            // Generate Visdata object using VisInfo
            VisData::ShPtr visData;
            visData = imgEq->initImagingEquations(griddingInfo);
            imgEq->calcImagingEquationsInvert(*ne,visData);

            // Send the resulting normal equation images to the output port
            if (has_output("Normal")) {
                NEUtils::sendNE(ne, output("Normal"));
            }

        }

        stats.logSummary();

        return 0;
    }

    void GridInvert::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void GridInvert::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }

} // namespace

