/// @file Ingest.cc
///
/// @details Implements a test method that uses the contents of the parset
/// to load in a measurement set and print a summary of its contents
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Stephen Ord <stephen.ord@csiro.au>
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "Ingest"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_Ingest() {
        return std::string("Ingest; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_Ingest()

#include <vector>
#include <mutex>

// Local includes
#include <daliuge/DaliugeApplication.h>
#include <factory/Ingest.h>
#include <factory/NEUtils.h>

// Yandasoft Data Source and Iterator classes (common to all data access methods that use Yandasoft DataAccessors)
#include <dataaccess/TableDataSource.h>
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>
#include <dataaccess/IDataConverter.h>
#include <dataaccess/IDataSelector.h>

// Yandasoft DataAccessors, DataIterators and Gridders
#include <measurementequation/ImageFFTEquation.h>
#include <gridding/IVisGridder.h>
#include <gridding/VisGridderFactory.h>

// Yandasoft DataAccessors with local GriddingInfo and Gridders
#include <factory/ImageEquation.h>
#include <factory/gridders/IVisGridder.h>
#include <factory/gridders/VisGridderFactory.h>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
// LOFAR Blob
#include <Blob/BlobString.h>
#include <Blob/BlobOBufString.h>
#include <Blob/BlobIBufString.h>
#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
#include <askap/StatReporter.h>

ASKAP_LOGGER(logger, ".Ingest");

// params helpers
#include <measurementequation/SynthesisParamsHelper.h>
#include <measurementequation/ImageParamsHelper.h>
#include <parallel/GroupVisAggregator.h>

#include <scimath/fitting/Params.h>

#include <string.h>
#include <sys/time.h>

namespace askap {

    Ingest::Ingest(dlg_app_info *raw_app) :
        DaliugeApplication(raw_app)
    {
        this->itsRank = NEUtils::getRank(raw_app->uid);
        if (this->itsRank < 0) {
            this->itsRank = 0;
        }
    }

    Ingest::~Ingest() {
    }

    DaliugeApplication::ShPtr Ingest::createDaliugeApplication(dlg_app_info *raw_app)
    {
        return Ingest::ShPtr(new Ingest(raw_app));
    }

    int Ingest::init(const char ***arguments) {

        while (1) {
            const char **param = *arguments;

            // Sentinel
            if (param == NULL) {
                break;
            }
            //else if (strcmp(param[0], "num_cpus") == 0) {
            //    // could test against num_of_copies, but just let dlg figure it out
            //}
            //else if (strcmp(param[0], "iid") == 0) {
            //    // iid = branch/rank should match the end of raw_app->uid and be consistent with getRank()
            //}
            //else if (strcmp(param[0], "rank") == 0) {
            //    // rank = [branch, rank] should be consistent with iid and raw_app->uid
            //}

            arguments++;
        }

        return 0;
    }

    int Ingest::run() {

#ifndef ASKAP_PATCHED
        static std::mutex safety;
#endif

        ASKAP_LOGGER(logger, ".Ingest.run");

        askap::StatReporter stats;

        // Lets get the key-value-parset
        char buf[64*1024];

        if (has_input("Config")) {
            size_t n_read = input("Config").read(buf, 64*1024);
            if (n_read == 0) {
                ASKAPLOG_WARN_STR(logger, "Nothing read from input Config");
                return -1;
            }
        } else {
            ASKAPLOG_WARN_STR(logger, "No input Config");
            return -1;
        }

        LOFAR::ParameterSet parset(true);
        parset.adoptBuffer(buf);

        // we need to fill the local parset with parameters that maybe missing
        try {
#ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
#endif
            NEUtils::finaliseParameters(parset, this->itsRank);
        }
        catch (std::runtime_error)
        {
            return -1;
        }
        this->itsParset = parset.makeSubset("Cimager.");

        // after finaliseParameters the following should be set and should be integers
        const std::vector<int> ChannelsParam = parset.getIntVector("Cimager.Channels");

        const string colName = this->itsParset.getString("datacolumn", "DATA");
        vector<std::string> ms = NEUtils::getDatasets(this->itsParset);
        ASKAPCHECK(ms.size()==1, "Only a single measurement set can be processed at this stage. You have "<<ms);

        {
            #ifndef ASKAP_PATCHED
            std::lock_guard<std::mutex> guard(safety);
            #endif

            // set up base-accessor Data Source and Iterator classes
            ASKAPCHECK(ms.size()==1, "Only a single measurement set can be processed at this stage.");
            ASKAPLOG_INFO_STR(logger, "Processing " << ms[0]);

            accessors::TableDataSource ds(ms[0], accessors::TableDataSource::DEFAULT, colName);

            // this is the data selector for the measurement set
            accessors::IDataSelectorPtr sel = ds.createSelector();

            sel->chooseCrossCorrelations();
            sel->chooseChannels(ChannelsParam[0], ChannelsParam[1]);

            ASKAPLOG_INFO_STR(logger,"Selecting "<<ChannelsParam[0]<<" channels starting at "<<ChannelsParam[1]);

            accessors::IDataConverterPtr conv = ds.createConverter();
            conv->setFrequencyFrame(casacore::MFrequency::Ref(casacore::MFrequency::TOPO), "Hz");
            conv->setDirectionFrame(casacore::MDirection::Ref(casacore::MDirection::J2000));
            conv->setEpochFrame();

            accessors::IDataSharedIter it = ds.createIterator(sel, conv);

            ASKAPLOG_INFO_STR(logger,"Not applying calibration");

            // data access must use the Jacal access layer
            const string dataAccess = this->itsParset.getString("gridder.dataaccess", "datapartitions");
            ASKAPCHECK(dataAccess == "datapartitions", "Only dataaccess=datapartitions is possible in Ingest");

            if ( this->itsParset.isDefined("gridder.partitiontype") ) {
                ASKAPLOG_INFO_STR(logger," - gridder partitiontype: "<<
                    this->itsParset.getString("gridder.partitiontype"));
            }

            // Define an imaging equation with empty sky grids
            // We are only ingesting, but for an img eq, so use imgEq functionality to set things up
            // First, create the empty model from the definition in the parameter set.
            askap::scimath::Params::ShPtr skyGrids(new scimath::Params(true));
            askap::synthesis::SynthesisParamsHelper::setUpImages(skyGrids, this->itsParset.makeSubset("Images."));
            // Then build the imaging equation
            ASKAPLOG_INFO_STR(logger,"Building FFT/measurement equation");
            boost::shared_ptr<ImageEquation> imgEq(new ImageEquation(*skyGrids, it, itsParset));
            ASKAPDEBUGASSERT(imgEq);

            // Send Vis Vector to the output port
            //NEUtils::sendVisVector(???, output("visVector"));

            // Generate Visdata object and fill VisInfo
            VisData::ShPtr visData;
            visData = imgEq->partitionData();

            // Convert polarisation state of the data
            // there should only be a single partition, but this won't hurt
            // @TODO incorporate this into partitionData()?
            const casacore::Vector<casacore::Stokes::StokesTypes> imagePols = visData->getImagePols();
            for (uint k = 0; k<visData->nPartitions(); ++k) {
                visData->visInfo(k)->convertPol(imagePols);
            }

            ASKAPLOG_INFO_STR(logger, "Packing and sending partition with "<<
                                      visData->visInfo(0)->nSample()<<" samples, "<<
                                      visData->visInfo(0)->nPol()<<" polarisations and "<<
                                      visData->visInfo(0)->nChannel()<<" channels");

            // Send VisInfo to the output port
            if (has_output("VisInfo")) {
                NEUtils::sendVisInfo(visData->visInfo(0), output("VisInfo"));
            }

        }

        stats.logSummary();

        return 0;
    }

    void Ingest::data_written(const char *uid, const char *data, size_t n) {
        dlg_app_running();
    }

    void Ingest::drop_completed(const char *uid, drop_status status) {
        dlg_app_done(APP_FINISHED);
    }

} // namespace

