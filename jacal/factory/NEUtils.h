/// @file NEUtils.h

#ifndef ASKAP_FACTORY_NEUTILS_H
#define ASKAP_FACTORY_NEUTILS_H

#include "rename.h"
#include <daliuge/DaliugeApplication.h>

#include <factory/gridders/VisInfo.h>
#include <factory/gridders/GriddingInfo.h>

#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>

// LOFAR ParameterSet
#include <Common/ParameterSet.h>
#include <scimath/fitting/ImagingNormalEquations.h>
#include <scimath/fitting/Params.h>

#include <casacore/casa/BasicSL.h>
#include <casacore/casa/aips.h>
#include <casacore/casa/OS/Timer.h>
#include <casacore/ms/MeasurementSets/MeasurementSet.h>
#include <casacore/ms/MeasurementSets/MSColumns.h>
#include <casacore/ms/MSOper/MSReader.h>
#include <casacore/casa/Arrays/ArrayIO.h>
#include <casacore/casa/iostream.h>
#include <casacore/casa/namespace.h>
#include <casacore/casa/Quanta/MVTime.h>


namespace askap {

    static inline
    unsigned long usecs(struct timeval *start, struct timeval *end)
    {
      return (end->tv_sec - start->tv_sec) * 1000000 + (end->tv_usec - start->tv_usec);
    }

    /*!
     * @brief set of static utility functions for the NE manipulation
     * @details These are just a set of static functions I use more than once
     */
    class NEUtils
    {
    public:

        NEUtils() = delete;
        ~NEUtils() = delete;

        // sends an NE to an app output
        static void sendNE(askap::scimath::ImagingNormalEquations::ShPtr itsNe, dlg_output_info &output);
        // gets an NE from an app input and puts it into the ShPtr
        static void receiveNE(askap::scimath::ImagingNormalEquations::ShPtr itsNe, dlg_input_info &input);

        // sends Params to an app output
        static void sendParams(askap::scimath::Params::ShPtr params, dlg_output_info &output);
        // gets Params from an app input and puts it into the ShPtr
        static void receiveParams(askap::scimath::Params::ShPtr params, dlg_input_info &input);

        // sends VisInfo to an app output
        static void sendVisInfo(VisInfo::ShPtr visInfo, dlg_output_info &output);
        // gets VisInfo from an app input and puts it into the ShPtr
        static void receiveVisInfo(VisInfo::ShPtr visInfo, dlg_input_info &input);

        // sends GriddingInfo to an app output
        static void sendGriddingInfo(GriddingInfo::ShPtr visInfo, dlg_output_info &output);
        // gets GriddingInfo from an app input and puts it into the ShPtr
        static void receiveGriddingInfo(GriddingInfo::ShPtr visInfo, dlg_input_info &input);

        // sends vis vector to an app output
        static void sendVis(std::vector<std::vector<std::complex<float> > > &vis, dlg_output_info &output);
        // gets vis vector from an app input
        static void receiveVis(std::vector<std::vector<std::complex<float> > > &vis,
                               const int nSample, const int nPol, dlg_input_info &input);

        static void setSpectralBand(LOFAR::ParameterSet& parset, const int rank);

        // add parameters that may be missing from a configuration file
        static void finaliseParameters(LOFAR::ParameterSet& fullparset, const int chan=0);

        static std::vector<std::string> getDatasets(const LOFAR::ParameterSet& parset);

        static int getRank(char *uid);

        static int getNChan(LOFAR::ParameterSet& parset);

        static double getChanWidth(LOFAR::ParameterSet& parset, int chan);

        static double getFrequency(LOFAR::ParameterSet& parset, int chan=0, bool barycentre=false);

        static std::vector<double> getFrequencies(LOFAR::ParameterSet& parset);

        // these are from Synparallel.

        /// @brief read the models a parset file to the given params object
        /// @details The model can be composed from both images and components. This
        /// method populates Params object by adding model data read from the parset file.
        /// The model is given by shared pointer because the same method can be used for both
        /// simulations and calibration (the former populates itsModel, the latter populates
        /// itsPerfectModel)
        /// @param[in] parset reference to a LOFAR parameter set
        /// @param[in] pModel shared pointer to the params object (must exist)

        static void readModels(const LOFAR::ParameterSet& parset, const scimath::Params::ShPtr &pModel);

        private:
    };

} // namespace askap


#endif //
