/// @file ImageEquation.h
///
/// @brief Equation for discrete Fourier transform of an image
/// @details Equation for discrete Fourier transform of an image using gridding
/// and FFTs, but with scope for flexibility (DFTs, IDG, etc.)
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_IMAGEEQUATION_H
#define ASKAP_FACTORY_IMAGEEQUATION_H

#include <factory/gridders/IVisGridder.h>
#include <factory/gridders/VisData.h>

#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>

#include <scimath/fitting/Params.h>
#include <scimath/fitting/ImagingEquation.h>
#include <scimath/utils/ChangeMonitor.h>

#include <measurementequation/IVisCubeUpdate.h>

#include <casacore/casa/aips.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Cube.h>
#include <casacore/coordinates/Coordinates/CoordinateSystem.h>

#include <map>

#include <boost/shared_ptr.hpp>

namespace askap
{

    /// @brief FFT-based image equations
    ///
    /// @details This class does predictions and calculates normal equations
    /// images. Parameter names are image.{i,q,u,v}.*
    /// The transforms are done using gridding and FFTs.
    /// @ingroup measurementequation
    class ImageEquation
    {
      public:

        /// @brief default constructor
        ImageEquation();

        /// Constructor with specified gridder and iterator
        /// @param ip Parameters
        /// @param idi Data iterator
        /// @param gridder Shared pointer to a gridder
        /// @param parset imager parameter set
        ImageEquation(const askap::scimath::Params& ip,
                      accessors::IDataSharedIter& idi,
                      IVisGridder::ShPtr gridder,
                      const LOFAR::ParameterSet& parset);

        /// Constructor without specifying gridder
        /// @param ip Parameters
        /// @param idi Data iterator
        /// @param parset imager parameter set
        ImageEquation(const askap::scimath::Params& ip,
                      accessors::IDataSharedIter& idi,
                      const LOFAR::ParameterSet& parset);

        /// Constructor without specifying iterator
        /// @param ip Parameters
        /// @param gridder Shared pointer to a gridder
        /// @param parset imager parameter set
        ImageEquation(const askap::scimath::Params& ip,
                      IVisGridder::ShPtr gridder,
                      const LOFAR::ParameterSet& parset);

        /// @brief Copy constructor
        ImageEquation(const ImageEquation& other);

        /// @brief Assignment operator
        ImageEquation& operator=(const ImageEquation& other);

        ~ImageEquation();

        /// @brief Access the parameters (const)
        const askap::scimath::Params& parameters() const;

        /// @brief Set the parameters to new values
        /// @param ip Parameters
        virtual void setParameters(const askap::scimath::Params& ip);

        /// @brief Predict (FFT then degrid) model visibilities
        //void predict();

        /// @brief Invert (grid then FFT) visibilities
        //void invert();

        /// @brief Calculate the normal equations: predict -> subtract -> invert, one row of vis at a time
        /// @param ne Normal equations
        void calcImagingEquations(askap::scimath::ImagingNormalEquations& ne);

        /// @brief Provide a VisData object to use during gridding and degridding
        /// @param gd shared pointer to the VisData object to lock in
        void setDataPartitions(VisData::ShPtr& gd) {itsVisData = gd;}

        /// @brief Interate through the data filling a VisData object with new data partitions
        /// @return shared pointer to the VisData object
        /// @note done in ImageEquation to access parameters() metadata
        VisData::ShPtr partitionData();

        /// @brief Initialise VisData and gridder for a data partition
        /// @return shared pointer to the VisData object
        /// @note done in ImageEquation to access parameters() metadata
        VisData::ShPtr initImagingEquations(boost::shared_ptr<VisInfo> &visInfo);

        /// @brief Initialise VisData and gridder for a data partition
        /// @return shared pointer to the VisData object
        /// @note done in ImageEquation to access parameters() metadata
        VisData::ShPtr initImagingEquations(boost::shared_ptr<GriddingInfo> &griddingInfo);

        /// @brief Invert part of the normal equation calc: predict -> subtract -> invert, one row of vis at a time
        /// @details Grid and image the residual visibilities.
        /// @param ne Normal equations
        /// @param visData visData object containing griddingInfo
        void calcImagingEquationsInvert(askap::scimath::ImagingNormalEquations& ne, const VisData::ShPtr &visData);

        /// @brief Predict part of the normal equation calc: predict -> subtract -> invert, one row of vis at a time
        /// @details Transform and degrid the model visibilities.
        /// @param ne Normal equations
        /// @param visData visData object containing griddingInfo
        void calcImagingEquationsPredict(askap::scimath::ImagingNormalEquations& ne, VisData::ShPtr &visData);

        /// @brief Clone this into a shared pointer
        /// @return shared pointer to a copy
        //ImageEquation::ShPtr clone();

        /// @brief assign a different iterator
        /// @details This is a temporary method to assign a different iterator.
        /// All this business is a bit ugly, but should go away when all
        /// measurement equations are converted to work with accessors.
        /// @param idi shared pointer to a new iterator
        //void setIterator(accessors::IDataSharedIter& idi);

        /// @brief define whether to use an alternative gridder for the PSF
        /// and/or the preconditioner function.
        /// @details We have an option to build the PSF using the default
        /// spheriodal function gridder or the box (nearest neighbour) gridder,
        /// i.e. no w-term and no primary beam is simulated, as an alternative
        /// to the same user-defined gridder as used for the model. Apart from
        /// speed, it probably makes the approximation better (i.e. removes
        /// some factors of spatial  dependence) and may lead to cleaner
        /// preconditioning. However it can filter out features of the W and AW
        /// kernels during preconditioning when robustness approaches uniform
        /// weighting. A separate preconditioner function can also be selected.
        /// @param[in] parset imager parameter set to check for PSF options.
        /// Current options: sphfuncforpsf, boxforpsf and preconditioner.preservecf.
        void useAlternativePSF(const LOFAR::ParameterSet& parset);

        /// @brief setup object function to update degridded visibilities
        /// @details For the parallel implementation of the measurement equation we need
        /// inter-rank communication. To avoid introducing cross-dependency of the measurement
        /// equation and the MPI one can use polymorphic object function to sum degridded visibilities
        /// across all required ranks in the distributed case and do nothing otherwise.
        /// By default, this class doesn't alter degridded visibilities.
        /// @param[in] obj new object function (or an empty shared pointer to turn this option off)
        void setVisUpdateObject(const boost::shared_ptr<askap::synthesis::IVisCubeUpdate> &obj);
     
        /// @brief shared pointer to paramters
        /// @details Due to caching, derived classes may need to know when
        /// the parameters of the equation have been updated. To track all
        /// updates, itsParams is made private. All changes to parameters are
        /// done via this method (including setParameters exposed to the user).
        /// @note This method allows non-const manipulation of the parameters, but
        /// not the change of the actual shared pointer class. The latter is done only
        /// inside this class and not in derived classes.
        /// @return non-const reference to the shared pointer
        virtual const askap::scimath::Params::ShPtr& rwParameters() const throw();
 
        /// @brief reference the given parameter object
        /// @details Sometimes it is handy to have a number of equations sharing exactly
        /// the same parameters and use reference semantics. A call to this method allows
        /// to reference itsParams member of this class to any given shared pointer to
        /// scimath::Params. In particular, a shared pointer can be obtained using rwParameters
        /// method. Use with caution. There is some legacy code/design which keep track of 
        /// changes in parameters by overriding rwParameters. This methanism will not work 
        /// correctly if referencing is used. Ideally, we want to convert this approach to
        /// use change monitors together with scimath::Params class.
        /// @param[in] params shared pointer to scimath::Params class to adopt in this equation
        void reference(const askap::scimath::Params::ShPtr &params);

        /// I want access to the gridders but I dont want to change them

        IVisGridder::ShPtr getResidualGridder(std::string name) const {
            return itsResidualGridders[name];
        };

        IVisGridder::ShPtr getPreconGridder(std::string name) const {
            return itsPreconGridders[name];
        };

        IVisGridder::ShPtr getPSFGridder(std::string name) const {
            return itsPSFGridders[name];
        };

      private:

        /// Parameters
        askap::scimath::Params::ShPtr itsParams;

        /// Pointer to prototype gridder
        IVisGridder::ShPtr itsGridder;

        /// Map of gridders for the model
        mutable std::map<string, IVisGridder::ShPtr> itsModelGridders;

        /// Map of gridders for the residuals
        mutable std::map<string, IVisGridder::ShPtr> itsResidualGridders;

        /// Map of PSF gridders
        mutable std::map<string, IVisGridder::ShPtr> itsPSFGridders;

        /// Map of Wiener preconditioning gridders
        mutable std::map<string, IVisGridder::ShPtr> itsPreconGridders;

        /// Map of coordinate systems
        mutable std::map<string, casacore::CoordinateSystem> itsCoordSystems;

        /// Iterator giving access to the data
        mutable accessors::IDataSharedIter itsIdi;

        /// @brief change monitors per image parameter
        /// @details These objects are used to determine whether a new
        /// initialise degrid is necessary (i.e. image or coordinate system
        /// has been updated since the last call).
        mutable std::map<std::string, askap::scimath::ChangeMonitor> itsImageChangeMonitors;

        /// @param[in] parset imager parameter set
        void init(const LOFAR::ParameterSet& parset);

        /// @brief true, if the PSF is built using the default spheroidal function gridder
        /// @details We have an option to build PSF using the default spheriodal function
        /// gridder, i.e. no w-term and no primary beam is simulated. Apart from speed,
        /// it probably makes the approximation better (i.e. removes some factors of spatial
        /// dependence). However it can filter out features of the W and AW kernels during
        /// preconditioning when robustness approaches uniform weighting.
        bool itsSphFuncPSFGridder;

        /// @brief true, if the PSF is built using the box (nearest neighbour) gridder
        /// @details We have an option to build PSF using the box (nearest neighbour)
        /// gridder, i.e. no w-term and no primary beam is simulated. Pros and cons
        /// are similar to the itsSphFuncPSFGridder option, however this gridder can lead
        /// to cleaner preconditioning (esp. as robustness approaches uniform weighting).
        bool itsBoxPSFGridder;

        bool itsUsePreconGridder;

        VisData::ShPtr itsVisData;

        std::string itsDataPartitionType;

        /// @brief if set, visibility cube will be passed through this object function
        /// @details For the parallel implementation of the measurement equation we need
        /// inter-rank communication. To avoid introducing cross-dependency of the measurement
        /// equation and the MPI one can use polymorphic object function to sum degridded visibilities
        /// across all required ranks in the distributed case and do nothing otherwise.
        boost::shared_ptr<askap::synthesis::IVisCubeUpdate> itsVisUpdateObject;
    };

}
#endif
