/// @file OnDemandBufferDataAccessor.cc
///
/// @brief an adapter to most methods of IConstDataAccessor with buffering
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "OnDemandBufferDataAccessor"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_OnDemandBufferDataAccessor() {
        return std::string("OnDemandBufferDataAccessor; ASKAPsoft==Unknown");
    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_OnDemandBufferDataAccessor()

// own includes
#include <factory/OnDemandBufferDataAccessor.h>

using namespace askap;
//using namespace askap::accessors;

/// construct an object linked with the given const accessor
/// @param[in] acc a reference to the associated accessor
OnDemandBufferDataAccessor::OnDemandBufferDataAccessor(const accessors::IConstDataAccessor &acc) :
      accessors::MetaDataAccessor(acc), itsUseBuffer(false) {}
  
/// Read-only visibilities (a cube is nRow x nChannel x nPol; 
/// each element is a complex visibility)
///
/// @return a reference to nRow x nChannel x nPol cube, containing
/// all visibility data
///
const casacore::Cube<casacore::Complex>& OnDemandBufferDataAccessor::visibility() const
{

  #ifdef _OPENMP
  boost::shared_lock<boost::shared_mutex> lock(itsMutex);
  if (itsUseBuffer) {
      lock.unlock();
      checkBufferSize();
      lock.lock();
  #else    
  if (itsUseBuffer) {
      checkBufferSize();
  #endif
      if (itsUseBuffer) {
          return itsBuffer;
      }
  }
  return getROAccessor().visibility();
}
	
/// Read-write access to visibilities (a cube is nRow x nChannel x nPol;
/// each element is a complex visibility)
///
/// @return a reference to nRow x nChannel x nPol cube, containing
/// all visibility data
///
casacore::Cube<casacore::Complex>& OnDemandBufferDataAccessor::rwVisibility()
{
  #ifdef _OPENMP
  boost::upgrade_lock<boost::shared_mutex> lock(itsMutex);
  if (itsUseBuffer) {
      lock.unlock();
      checkBufferSize();
      lock.lock();
  #else
  if (itsUseBuffer) {
      checkBufferSize();
  #endif
  }
  // itsUseBuffer may be changed by the call to checkBufferSize
  if (!itsUseBuffer) {
      #ifdef _OPENMP
      boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);
      #endif
      itsBuffer = getROAccessor().visibility().copy();
      itsUseBuffer = true;
  }
  return itsBuffer;  
}

/// @brief a helper method to check whether the buffer has a correct size
/// @details The wrong size means that the iterator has advanced and this
/// accessor has to be coupled back to the read-only accessor which has been given at the 
/// construction. If a wrong size is detected, itsUseBuffer flag is reset.
void OnDemandBufferDataAccessor::checkBufferSize() const
{
  #ifdef _OPENMP
  boost::shared_lock<boost::shared_mutex> lock(itsMutex);
  #endif
  const accessors::IConstDataAccessor &acc = getROAccessor();
  if (itsBuffer.nrow() != acc.nRow() || itsBuffer.ncolumn() != acc.nChannel() ||
                                        itsBuffer.nplane() != acc.nPol()) {
      // couple the class to the original accessor
      // discardCache operates with just mutable data members. Although technically discardCache
      // can be made a const method, it is probably conceptually wrong. Therefore, we take the
      // constness out, instead.
      #ifdef _OPENMP
      lock.unlock();
      #endif
      const_cast<OnDemandBufferDataAccessor*>(this)->discardCache(); 
  }
}

/// @brief discard the content of the cache
/// @details A call to this method would switch the accessor to the pristine state
/// it had straight after construction. A new call to rwVisibility would be required 
/// to decouple from the read-only accessor 
void OnDemandBufferDataAccessor::discardCache()
{
  #ifdef _OPENMP
  boost::unique_lock<boost::shared_mutex> lock(itsMutex);
  #endif
  itsUseBuffer = false;
  itsBuffer.resize(0,0,0);
}

