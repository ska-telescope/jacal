/// @file BoxVisGridder.h
///
/// BoxVisGridder: Box-based visibility gridder.
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "BoxVisGridder"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_BoxVisGridder() {
        return std::string("BoxVisGridder; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_BoxVisGridder()

#include <factory/gridders/BoxVisGridder.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
ASKAP_LOGGER(logger, ".BoxVisGridder");
#include <profile/AskapProfiler.h>

namespace askap
{

    BoxVisGridder::BoxVisGridder()
    {
    }

    BoxVisGridder::~BoxVisGridder()
    {
    }

    /// Clone a copy of this Gridder
    IVisGridder::ShPtr BoxVisGridder::clone()
    {
        return IVisGridder::ShPtr(new BoxVisGridder(*this));
    }

    /// @brief static method to create gridder
    /// @details Each gridder should have a static factory method, which is
    /// able to create a particular type of the gridder and initialise it with
    /// the parameters taken form the given parset. It is assumed that the
    /// method receives a subset of parameters where the gridder name is already
    /// taken out.
    /// @return a shared pointer to the gridder instance
    IVisGridder::ShPtr BoxVisGridder::createGridder(const LOFAR::ParameterSet&)
    {
        return IVisGridder::ShPtr(new BoxVisGridder());
    }

    void BoxVisGridder::initIndices(const boost::shared_ptr<VisInfo>&)
    {
    }

    void BoxVisGridder::initConvolutionFunction(const boost::shared_ptr<VisInfo>&)
    {
        itsSupport=0;
        itsOverSample=1;
        const int cSize=2*itsSupport+1; // 1
        const int cCenter=(cSize-1)/2; // 0
        itsConvFunc.resize(1);
        itsConvFunc[0].resize(cSize, cSize); // 1, 1
        itsConvFunc[0].set(0.0);
        itsConvFunc[0](cCenter,cCenter)=1.0; // 0,0 = 0
    }

    void BoxVisGridder::correctConvolution(casacore::Array<imtype>& /*image*/)
    {
    }

}
