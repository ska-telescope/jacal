/// @file GriddingInfo.cc
///
/// @brief 
/// @details 
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "GriddingInfo"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_GriddingInfo() {
        return std::string("GriddingInfo; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_GriddingInfo()

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
#include <profile/AskapProfiler.h>

// Base Accessors
#include <dataaccess/IConstDataAccessor.h>

// Local includes
#include <factory/gridders/GriddingInfo.h>

#include <casacore/casa/BasicSL/Constants.h>

#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>
#include <Blob/BlobArray.h>
#include <Blob/BlobSTL.h>

namespace askap {

GriddingInfo::GriddingInfo() :
                      itsNumberOfSamples(0), itsNumberOfPols(0), itsNumberOfChannels(0) {}

/// @brief
void GriddingInfo::initModel()
{
    itsModel.resize(itsNumberOfSamples);
    for (uint i=0; i<itsNumberOfSamples; ++i) {
        itsModel[i].resize(itsNumberOfPols);
    }
}

/// @brief
void GriddingInfo::subtractModel()
{
    for (uint i=0; i<itsNumberOfSamples; ++i) {
        for (uint pol=0; pol<itsNumberOfPols; ++pol) {
            itsSample[i][pol] -= itsModel[i][pol];
            // reset model to zero?
            //itsModel[i][pol] = 0.0;
        }
    }
}

/// @brief
void GriddingInfo::resetModel()
{
    for (uint i=0; i<itsNumberOfSamples; ++i) {
        for (uint pol=0; pol<itsNumberOfPols; ++pol) {
            itsModel[i][pol] = 0.0;
        }
    }
}


/// @brief Update the visibility data sample vector
/// @param reference to the new samples
void GriddingInfo::updateVis(std::vector<std::vector<std::complex<float> > > &vis)
{
    ASKAP_LOGGER(logger, ".GriddingInfo.updateVis");
    ASKAPCHECK(vis.size() > 0, "Input vis has length "<<itsSample.size());
    if (itsSample.size() == 0) {
        ASKAPLOG_INFO_STR(logger, "GriddingInfo vis are unset. Referencing new vis.");
        // DAM TODO this is a copy, but should be a reference
        itsSample = vis;
    } else {
        ASKAPLOG_INFO_STR(logger, "GriddingInfo vis already set. Replacing with new vis.");
        ASKAPCHECK(vis.size() == itsNumberOfSamples, "Vis length error. "<<vis.size()<<" != "<<itsNumberOfSamples);
        ASKAPCHECK(vis[0].size() == itsNumberOfPols, "Vis length error. "<<vis[0].size()<<" != "<<itsNumberOfPols);
        itsSample = vis;
    }
}


/// @brief Obtain offset for the given convolution function
/// @details To conserve memory and speed the gridding up, convolution functions stored in the cache
/// may have an offset (i.e. essentially each CF should be defined on a bigger support and placed at a
/// pixel other than the centre of this support). This method returns this offset, which is the
/// position of the peak of the given CF on a bigger support w.r.t. the centre of this support.
/// The value of (0,0) means no offset from the centre (i.e. support is already centred).
/// @param[in] cfPlane plane of the convolution function cache to get the offset for
/// @return a pair with offsets for each axis
/// @note if there is no offset defined for a given cfPlane (default behavior), this method returns (0,0)
std::pair<int,int> GriddingInfo::getConvFuncOffset(int cfPlane) const
{
  ASKAPDEBUGASSERT(cfPlane>=0);
  if (cfPlane >= int(itsConvFuncOffsets.size())) {
      return std::pair<int,int>(0,0);
  }
  return itsConvFuncOffsets[cfPlane];
}


// These are the items that we need to write to and read from a blob stream
// std::map<std::string, casacore::Array<double> > itsArrays;
// std::map<std::string, Axes> itsAxes;
// std::map<std::string, bool> itsFree;

#define BLOBVERSION 0

LOFAR::BlobOStream& operator<<(LOFAR::BlobOStream& os, const GriddingInfo& info)
{
    // Copy the cache of gridding kernels to a suitable format.
    // This will be removed once a final cache format has been chosen.
    // DAM check out ImagingNormalEquations and <askap/scimath/utils/CasaBlobUtils.h>
    const int nPlanes = info.itsConvFunc.size();
    std::vector<int> cSize(nPlanes);
    //std::vector<casacore::Array<casacore::Complex> > tmpConvFunc(nPlanes);
    std::vector<std::vector<casacore::Complex> > tmpConvFunc(nPlanes);
    for (uint plane = 0; plane < nPlanes; ++plane) {
        cSize[plane] = info.itsConvFunc[plane].nrow();
        // test that info.itsConvFunc[plane].nrow() = info.itsConvFunc[plane].ncolumn()
        tmpConvFunc[plane].resize(cSize[plane]*cSize[plane]);
        for (uint j = 0; j < cSize[plane]; ++j) {
            for (uint i = 0; i < cSize[plane]; ++i) {
                tmpConvFunc[plane][j*cSize[plane]+i] = info.itsConvFunc[plane](i,j);
            }
        }
    }
    os.putStart("GriddingInfo",BLOBVERSION);
    os << info.itsNumberOfSamples
       << info.itsNumberOfChannels
       << info.itsNumberOfPols
       << info.itsGridIndexU
       << info.itsGridIndexV
       //<< info.itsGridIndexW
       << info.itsKernelIndex
       << info.itsPhasor
       << info.itsChannel
       << info.itsFlag
       << info.itsWeight
       << nPlanes
       << cSize
       << tmpConvFunc;
	os.putEnd();
    return os;
}

LOFAR::BlobIStream& operator>>(LOFAR::BlobIStream& is, GriddingInfo& info)
{
    // info should still have default constructor settings
    ASKAPDEBUGASSERT(info.itsNumberOfSamples == 0);
    ASKAPDEBUGASSERT(info.itsNumberOfChannels == 0);
    ASKAPDEBUGASSERT(info.itsNumberOfPols == 0);
    const int version = is.getStart("GriddingInfo");
    ASKAPCHECK(version == BLOBVERSION,
        "Attempting to read from a blob stream for a GriddingInfo object of the wrong version, expect "<<
        BLOBVERSION<<" got "<<version);

    // first get size parameters and resize the vectors
    is >> info.itsNumberOfSamples
       >> info.itsNumberOfChannels
       >> info.itsNumberOfPols;

    ASKAPDEBUGASSERT(info.itsNumberOfSamples > 0);
    ASKAPDEBUGASSERT(info.itsNumberOfChannels > 0);
    ASKAPDEBUGASSERT(info.itsNumberOfPols > 0);

    // allocate memory for later
    info.itsStokes.resize(info.itsNumberOfPols);
    info.itsFrequencyList.resize(info.itsNumberOfChannels);

    // allocate memory to accept blobs
    info.itsGridIndexU.resize(info.itsNumberOfSamples);
    info.itsGridIndexV.resize(info.itsNumberOfSamples);
    //info.itsGridIndexW.resize(info.itsNumberOfSamples);
    info.itsKernelIndex.resize(info.itsNumberOfSamples);
    info.itsPhasor.resize(info.itsNumberOfSamples);
    info.itsChannel.resize(info.itsNumberOfSamples);
    info.itsFlag.resize(info.itsNumberOfSamples);
    info.itsWeight.resize(info.itsNumberOfSamples);
    for (uint i=0; i<info.itsNumberOfSamples; ++i) {
        info.itsWeight[i].resize(info.itsNumberOfPols);
    }

    is >> info.itsGridIndexU
       >> info.itsGridIndexV
       //>> info.itsGridIndexW
       >> info.itsKernelIndex
       >> info.itsPhasor
       >> info.itsChannel
       >> info.itsFlag
       >> info.itsWeight;

    int nPlanes;
    is >> nPlanes;
    std::vector<int> cSize(nPlanes);
    is >> cSize;
    std::vector<std::vector<casacore::Complex> > tmpConvFunc(nPlanes);
    for (uint plane = 0; plane < nPlanes; ++plane) {
        tmpConvFunc[plane].resize(cSize[plane]*cSize[plane]);
    }
    is >> tmpConvFunc;
    // Copy the cache of gridding kernels back to the required format.
    // This will be removed once a final cache format has been chosen.
    info.itsConvFuncOffsets.resize(nPlanes);
    info.itsConvFunc.resize(nPlanes);
    for (uint plane = 0; plane < nPlanes; ++plane) {
        info.itsConvFunc[plane].resize(cSize[plane],cSize[plane]);
        for (uint j = 0; j < cSize[plane]; ++j) {
            for (uint i = 0; i < cSize[plane]; ++i) {
                info.itsConvFunc[plane](i,j) = tmpConvFunc[plane][j*cSize[plane]+i];
            }
        }
    }

    is.getEnd();
    return is;
}

GriddingInfo::~GriddingInfo()
{
}

} // end of namespace askap
