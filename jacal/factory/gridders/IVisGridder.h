/// @file IVisGridder.h
///
/// @brief Base class for visibility gridders.
///
/// @author Max Voronkov <maxim.voronkov@csiro.au>

#ifndef ASKAP_FACTORY_IVISGRIDDER_H
#define ASKAP_FACTORY_IVISGRIDDER_H

#include <factory/gridders/VisData.h>

#include <casacore/casa/aips.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Cube.h>

#include <scimath/fitting/Axes.h>
#include <askap/AskapImageType.h>

#include <boost/shared_ptr.hpp>

#include <Common/ParameterSet.h>

#include <gridding/IVisWeights.h>
#include <string>

namespace askap
{

    /// @brief Abstract Base Class for all gridders.
    /// A gridder puts the synthesis data onto a grid and transforms
    /// as necessary. To allow all the important possibilities, the
    /// Fourier transforms are performed here rather than externally.
    ///
    /// There is a separate class for degridding.
    /// @ingroup gridding
    class IVisGridder
    {
        public:

        /// Shared pointer definition
        typedef boost::shared_ptr<IVisGridder> ShPtr;

        IVisGridder();
        virtual ~IVisGridder();

        /// Clone a copy of this Gridder
        virtual ShPtr clone() = 0;

        /// @brief Initialise resources common to gridding and degridding
        /// @param visData visdata that have been initialised with specific grid metadata
        virtual void initialiseCommon(const VisData::ShPtr& visData) = 0;

        /// @brief Further initialisation specific to gridding
        /// @param visData visdata that have been initialised with specific grid metadata
        /// @param dopsf Make the psf?
        virtual void initialiseGrid(const VisData::ShPtr& visData, const bool dopsf=true, const bool dopcf=false) = 0;

        /// @brief Further initialisation specific to degridding
        /// @param visData visdata that have been initialised with specific grid metadata
        /// @param image Input image: cube: u,v,pol,chan
        virtual void initialiseDegrid(const VisData::ShPtr& visData, const casacore::Array<imtype>& image) = 0;

        /// @brief set visibility weights
        /// @param[in] viswt shared pointer to visibility weights
        virtual void initVisWeights(const askap::synthesis::IVisWeights::ShPtr &viswt) = 0;

        /// @brief Make context-dependant changes to the gridder behaviour.
        /// @details e.g. set Taylor term indices
        /// @param[in] context context description
        virtual void customiseForContext(const std::string &context) = 0;

        /// @brief Grid the visibility data.
        /// @param gdp GriddingInfo object to work with
        virtual void grid(boost::shared_ptr<GriddingInfo>& griddingInfo) = 0;

        /// @brief Degrid the visibility data.
        /// @param[in] gdp GriddingInfo object to work with
        virtual void degrid(boost::shared_ptr<GriddingInfo>& griddingInfo) = 0;

        /// Form the final output image
        /// @param out Output double precision image or PSF
        virtual void finaliseGrid(casacore::Array<imtype>& out) = 0;

        /// @brief Finalise
        virtual void finaliseDegrid() = 0;

        /// Form the sum of the convolution function squared, multiplied by the weights for each
        /// different convolution function. This is used in the evaluation of the second derivative.
        /// @param out Output double precision sum of weights images
        virtual void finaliseWeights(casacore::Array<imtype>& out) = 0;

        /// @brief static method to create gridder
        /// @details Each gridder should have a static factory method, which is
        /// able to create a particular type of the gridder and initialise it with
        /// the parameters taken form the given parset. It is assumed that the
        /// method receives a subset of parameters where the gridder name is already
        /// taken out.
        /// @param[in] parset input parset file
        /// @return a shared pointer to the gridder instance
        /// @note This method just throws an exception in this basic interface. It is
        /// added to ensure that all derived classes have this method defined. We have
        /// to use a static method as opposed to pure virtual function because we plan to use it
        /// to create a brand new instance of the gridder (and hence no object would
        /// exist at that stage)
        static ShPtr createGridder(const LOFAR::ParameterSet& parset);

        /// @brief check whether the model is empty
        /// @details A simple check allows us to bypass heavy calculations if the input model
        /// is empty (all pixels are zero). This makes sense for degridding only.
        /// @brief true, if the model is empty
        virtual bool isModelEmpty() const = 0;
    };

}
#endif                                            /*ASKAP_FACTORY_IVISGRIDDER_H*/
