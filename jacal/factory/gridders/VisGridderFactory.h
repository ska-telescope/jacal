/// @file VisGridderFactory.h
///
/// @brief Factory class for visibility gridders.
/// @details This is a cutdown version of the Yandasoft factory
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_VISGRIDDERFACTORY_H
#define ASKAP_FACTORY_VISGRIDDERFACTORY_H

#include <factory/gridders/IVisGridder.h>

// System includes
#include <map>

// 3rd party includes
#include <Common/ParameterSet.h>
#include <casacore/scimath/Mathematics/Interpolate2D.h>
#include <boost/shared_ptr.hpp>

namespace askap
{

    /// @brief Factory class for visibility gridders
    /// @ingroup gridding
    class VisGridderFactory
    {
    public:
      /// @brief Signature of a function creating a Gridder object.
      /// All functions creating a IVisGridder object must have
      /// this signature. Preferably such a function is a static
      /// function in that gridder class.
      typedef IVisGridder::ShPtr GridderCreator(const LOFAR::ParameterSet&);

      /// @brief Register a function creating a gridder object.
      /// @param name The name of the gridder.
      /// @param creatorFunc pointer to creator function.
      static void registerGridder(const std::string& name, GridderCreator* creatorFunc);

      /// @brief Try to create a non-standard gridder.
      /// Its name is looked up in the creator function registry.
      /// If the gridder name is unknown, a shared library with that name
      /// (in lowercase) is loaded and it executes its register<name>
      /// function which must register its creator function in the registry
      /// using function registerGridder.
      /// @param name The name of the gridder.
      /// @param parset ParameterSet containing description of
      /// gridder to be constructed
      static IVisGridder::ShPtr createGridder(const std::string& name,
                                                                const LOFAR::ParameterSet& parset);


      /// @brief Factory class for all gridders.
      /// @todo Python version of factory 
      VisGridderFactory();

      /// @brief Make a shared pointer for a visibility gridder
      /// @param parset ParameterSet containing description of
      /// gridder to be constructed.
      /// If needed, the gridder code is loaded dynamically.
      static IVisGridder::ShPtr make(const LOFAR::ParameterSet& parset);

    protected:
      /// @brief helper template method to add pre-defined gridders
      template<typename GridderType> static inline void addPreDefinedGridder()  {
          registerGridder(GridderType::gridderName(), GridderType::createGridder);
      }
      
      
    private:
      static std::map<std::string, GridderCreator*> theirRegistry;

    };

}
#endif 

