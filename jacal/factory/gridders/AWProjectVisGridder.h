/// @file AWProjectVisGridder.h
///
/// @brief AW projection gridding
///
/// @details Grids visibility data using the self-convolution of
/// the antenna illumination pattern.
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_AWPROJECTVISGRIDDER_H_
#define ASKAP_FACTORY_AWPROJECTVISGRIDDER_H_

// Local includes
#include <factory/gridders/WProjectVisGridder.h>
#include <factory/gridders/AProjectGridderBase.h>
#include <factory/gridders/VisInfo.h>

// ASKAPsoft includes
#include <boost/shared_ptr.hpp>

// ASKAPsoft includes
#include <gridding/IBasicIllumination.h>

namespace askap
{
    /// @brief Gridder that is appropriate for mosaicing.
    ///
    /// @details The visibilities are gridded using a convolution
    /// function derived from the antenna illumination pattern,
    /// appropriately shifted in position for each feed, and
    /// incorporating the Fresnel term needed to correct for the
    /// w-term in the full measurement equation.
    ///
    /// The scaling is slow in data points, slow in w planes
    /// (since the calculation of the convolution function
    /// usually dominates).
    ///
    /// @ingroup gridding
    class AWProjectVisGridder : public WProjectVisGridder, virtual protected AProjectGridderBase
    {
        public:

            /// @brief Construct antenna illumination pattern/W term gridder
            /// @param illum  Antenna illumination model
            /// @param wmax Maximum baseline (wavelengths)
            /// @param nwplanes Number of w planes
            /// @param cutoff Cutoff in determining support e.g. 10^-3 of the peak
            /// @param overSample Oversampling (currently limited to <=1)
            /// @param maxSupport Maximum support to be allowed
            /// @param limitSupport Upper limit of support
            /// @param maxFeeds Maximum number of feeds allowed
            /// @param maxFields Maximum number of fields allowed
            /// @param pointingTol Pointing tolerance in radians
            /// @param paTol Parallactic angle tolerance in radians
            /// @param freqTol Frequency tolerance (relative, threshold for df/f), negative value
            ///        means the frequency axis is ignored
            /// @param frequencyDependent Frequency dependent gridding?
            /// @param name Name of table to save convolution function into
            AWProjectVisGridder(const boost::shared_ptr<askap::synthesis::IBasicIllumination const> &illum,
                    const double wmax, const int nwplanes, const double cutoff,
                    const int overSample, const int maxSupport, const int limitSupport,
                    const int maxFeeds=1, const int maxFields=1, const double pointingTol=0.0001,
                    const double paTol=0.01,
                    const double freqTol = 1e-6,
                    const bool frequencyDependent=true,
                    const std::string& name=std::string(""));

            /// @brief copy constructor
            /// @details It is required to decouple internal array arrays, otherwise
            /// those arrays are shared between all cloned gridders of this type
            /// @param[in] other input object
            /// @note illumination model is copied as a pointer, so the same model is referenced
            AWProjectVisGridder(const AWProjectVisGridder &other);

            /// Clone a copy of this Gridder
            virtual IVisGridder::ShPtr clone();

            /// Form the sum of the convolution function squared, multiplied by the weights for each
            /// different convolution function. This is used in the evaluation of the second derivative.
            /// @param out Output double precision grid
            virtual void finaliseWeights(casacore::Array<imtype>& out);

            /*
            /// Form the final output image or PSF
            /// @param out Output double precision image or PSF
            virtual void finaliseGrid(casacore::Array<imtype>& out);
            */

            /// @brief Initialise resources common to gridding and degridding
            /// @param visData visdata that have been initialised with specific grid metadata
            virtual void initialiseCommon(const VisData::ShPtr& visData);

            /// @brief Further initialisation specific to gridding
            /// @param axes axes specifications
            /// @param shape Shape of output image: u,v,pol,chan
            /// @param dopsf Make the psf?
            virtual void initialiseGrid(const VisData::ShPtr& visData, const bool dopsf=true, const bool dopcf=false);

            /// @brief Further initialisation specific to degridding
            /// @param axes axes specifications
            /// @param image Input image: cube: u,v,pol,chan
            virtual void initialiseDegrid(const VisData::ShPtr& visData, const casacore::Array<imtype>& image);

            /// @brief static method to get the name of the gridder
            /// @details We specify parameters per gridder type in the parset file.
            /// This method returns the gridder name which should be used to extract
            /// a subset of parameters for createGridder method.
            static inline std::string gridderName() { return "AWProject";}

            /// @brief static method to create gridder
            /// @details Each gridder should have a static factory method, which is
            /// able to create a particular type of the gridder and initialise it with
            /// the parameters taken form the given parset. It is assumed that the
            /// method receives a subset of parameters where the gridder name is already
            /// taken out.
            /// @param[in] parset input parset file
            /// @return a shared pointer to the gridder instance
            static IVisGridder::ShPtr createGridder(const LOFAR::ParameterSet& parset);

        protected:
            /// @brief initialise sum of weights
            /// @details We keep track the number of times each convolution function is used per
            /// channel and polarisation (sum of weights). This method is made virtual to be able
            /// to do gridder specific initialisation without overriding initialiseGrid.
            /// This method accepts no parameters as itsShape, itsNWPlanes, etc should have already
            /// been initialised by the time this method is called.
            virtual void initialiseSumOfWeights();

            /// @brief Initialise the indices
            /// @param[in] acc const accessor to work with
            virtual void initIndices(const boost::shared_ptr<VisInfo>& visInfo);

            /// Index into convolution function
            /// @param row Row number
            /// @param pol Polarization id
            /// @param chan Channel number
            virtual int cIndex(int index);

            /// Initialize convolution function
            /// @param[in] acc const accessor to work with
            virtual void initConvolutionFunction(const boost::shared_ptr<VisInfo>& visInfo);

            /// Correct for gridding convolution function
            /// @param image image to be corrected
            virtual void correctConvolution(casacore::Array<imtype>& image);

            /// @brief whether to iterpolate the spheroidal function at nu=1
            /// @details The function is undefined and set to zero at nu=1,
            /// but that is not the numerical limit. Setting itsInterp true
            /// will use neighbouring values to estimate it (to 2nd order).
            bool itsInterp;

        private:
            /// @brief assignment operator (not to be called)
            /// @details It is made private, so we can't call it inadvertently
            /// @param[in] other input object
            AWProjectVisGridder& operator=(const AWProjectVisGridder &other);

            /// Reference frequency for illumination pattern.
            double itsReferenceFrequency;

            /// Antenna illumination model
            boost::shared_ptr<askap::synthesis::IBasicIllumination const> itsIllumination;

            /// Is the convolution function frequency dependent?
            bool itsFreqDep;
                   
            /// @brief flag showing that CFs are rebuilt due to parallactic angle
            bool itsCFInvalidDueToPA;
  
            /// @brief flag showing that CFs are rebuilt due to frequency axis change
            bool itsCFInvalidDueToFreq; 

    };

} // end namespace askap
#endif
