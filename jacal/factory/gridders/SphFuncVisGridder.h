/// @file SphFuncVisGridder.h
///
/// @brief Prolate spheroidal function-based visibility gridder
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>

#ifndef ASKAP_FACTORY_SPHVISGRIDDER_H
#define ASKAP_FACTORY_SPHVISGRIDDER_H

#include <factory/gridders/VisGridder.h>
#include <factory/gridders/VisInfo.h>

#include <scimath/utils/SpheroidalFunction.h>

namespace askap
{

    /// @brief SphFuncVisGridder: Spheroidal function-based visibility gridder.
    /// @details The gridding function is a prolate spheroidal function identical to the
    /// one used in AIPS, AIPS++, and probably other packages. At some point
    /// we should revisit the tradeoffs since the choice to use this was made
    /// about twenty years ago and computers are quite different now.
    ///
    /// The spheroidal function has m = 6, alpha = 1 using the rational
    /// approximations discussed by fred schwab in 'indirect imaging'.
    /// The gridding function is (1-nu**2)*grdsf(nu) where nu is the distance
    /// to the edge. the grid correction function is just 1/grdsf(nu) where nu
    /// is now the distance to the edge of the image.
    /// @ingroup gridding
    class SphFuncVisGridder : public VisGridder
    {
      public:

        /// @brief Standard two dimensional gridding
        /// @param[in] alpha spheroidal function alpha value
        /// @param[in] support support size in pixels (spheroidal
        /// function with m=2*support will be generated)
        /// @param[in] oversample number of oversampling planes
        explicit SphFuncVisGridder(const float alpha = 1.,
                                   const int support = 3,
                                   const int oversample = 128);

        virtual ~SphFuncVisGridder();

        /// Clone a copy of this Gridder
        virtual IVisGridder::ShPtr clone();

        /// @brief static method to get the name of the gridder
        /// @details We specify parameters per gridder type in the parset file.
        /// This method returns the gridder name which should be used to extract
        /// a subset of parameters for createGridder method.
        static inline std::string gridderName() { return "SphFunc";}

        /// @brief static method to create gridder
        /// @details Each gridder should have a static factory method, which is
        /// able to create a particular type of the gridder and initialise it with
        /// the parameters taken form the given parset. It is assumed that the
        /// method receives a subset of parameters where the gridder name is already
        /// taken out.
        /// @param[in] parset input parset file
        /// @return a shared pointer to the gridder instance
        static IVisGridder::ShPtr createGridder(const LOFAR::ParameterSet& parset);


      protected:
        /// @brief Initialize the convolution function
        /// @param[in] visInfo const VisInfo to work with
        virtual void initConvolutionFunction(const boost::shared_ptr<VisInfo>& visInfo);

        /// @brief Initialise the indices
        /// @param[in] visInfo const VisInfo to work with
        virtual void initIndices(const boost::shared_ptr<VisInfo>& visInfo);

        /// Correct for gridding convolution function
        /// @param image image to be corrected
        virtual void correctConvolution(casacore::Array<imtype>& image);

        /// Calculate prolate spheroidal function
        /// @param nu Argument for spheroidal function
        inline double grdsf(double nu) const { return itsSphFunc(nu); }

        //double grdsf1(double nu) const;

        /// @brief calculator of spheroidal function
        scimath::SpheroidalFunction itsSphFunc;

        /// @brief whether to iterpolate the spheroidal function at nu=1
        /// @details The function is undefined and set to zero at nu=1,
        /// but that is not the numerical limit. Setting itsInterp true
        /// will use neighbouring values to estimate it (to 2nd order).
        bool itsInterp;

        /// @brief iterpolate the spheroidal function at nu=1
        /// @details The function is undefined and set to zero at nu=1,
        /// but that is not the numerical limit. Setting itsInterp true
        /// will use neighbouring values to estimate it (to 2nd order).
        /// @param[in] func vector to be interpolated
        template<typename T>
        void interpolateEdgeValues(casacore::Vector<T> &func)
        {
          ASKAPDEBUGASSERT(itsInterp);
 
          int length = func.shape()[0];
          ASKAPASSERT(length>3);
 
          //func(0) = func(1) + (func(1)-func(2)) + (func(1)-2.0*func(2)+func(3));
          func(0) = T(3.) * (func(1) - func(2)) + func(3);
          if (length%2==1) { // nu=1 for the last element as well
            func(length-1) = func(0);
          }
        }

      private:

        /// @brief prolate spheroidal alpha parameter
        double itsAlpha;

    };

}

#endif
