/// @file VisGridder.cc
///
/// @brief Table-based visibility gridder/degridder
///
/// @author Tim Cornwell <tim.cornwell@csiro.au>
/// @author Daniel Mitchell <tim.cornwell@csiro.au>

// for logging
#define ASKAP_PACKAGE_NAME "VisGridder"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_VisGridder() {
        return std::string("VisGridder; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_VisGridder()

#include <factory/gridders/VisGridder.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>

#include <askap/CasaSyncHelper.h>
#include <askap/AskapUtil.h>

#include <scimath/utils/PaddingUtils.h>
#include <scimath/utils/ImageUtils.h>

#include <measurementequation/ImageParamsHelper.h>

#include <profile/AskapProfiler.h>


#include <scimath/fft/FFTWrapper.h>
#include <scimath/fitting/Params.h>
#include <scimath/fitting/ParamsCasaTable.h>

#include <casacore/casa/BasicSL/Constants.h>
#include <casacore/casa/Arrays/ArrayIter.h>
#include <casacore/casa/Arrays/ArrayMath.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/Slice.h>

#include <casacore/measures/Measures/MDirection.h>
#include <casacore/measures/Measures/UVWMachine.h>

using namespace askap::scimath;
using namespace askap;

#include <ostream>
#include <sstream>
#include <iomanip>

#include <casacore/casa/OS/Timer.h>

namespace askap {

/// @brief a helper method for a deep copy of casa arrays held in stl vectors
/// @param[in] in input array
/// @param[out] out output array (will be resized)
template<typename T>
void deepCopyOfSTDVector(const std::vector<T> &in, std::vector<T> &out)
{
   out.resize(in.size());

   const typename std::vector<T>::const_iterator inEnd = in.end();
   typename std::vector<T>::iterator outIt = out.begin();
   for (typename std::vector<T>::const_iterator inIt = in.begin();
       inIt != inEnd; ++inIt,++outIt) {
       *outIt = inIt->copy();
   }
}

/// @brief required to mediate thread safety issues of the casa cube
utility::CasaSyncHelper syncHelper;

VisGridder::VisGridder() :
    itsSumWeights(), itsPaddingFactor(1.0), itsSupport(-1), itsOverSample(-1),
    itsModelIsEmpty(false), itsSamplesGridded(0), itsSamplesDegridded(0),
    itsVectorsFlagged(0), itsVectorsWFlagged(0), itsNumberGridded(0), itsNumberDegridded(0),
    itsTimeCoordinates(0.0), itsTimeConvFunctions(0.0), itsTimeGridded(0.0),
    itsTimeDegridded(0.0), itsDopsf(false), itsDopcf(false),
    itsTrackWeightPerOversamplePlane(false),itsPARotation(false),itsSwapPols(false),
    its2dGrid(),itsVisPols(),itsPolConv(),itsImagePolFrameVis(),itsImagePolFrameNoise(),
    itsPolVector(),itsImageChan(-1),itsGridIndex(-1),itsClearGrid(false)
{
}

VisGridder::VisGridder(const int overSample, const int support, const float padding,
                       const std::string& name) :
    itsSumWeights(), itsPaddingFactor(padding), itsSupport(support), itsOverSample(overSample),
    itsModelIsEmpty(false), itsName(name), itsSamplesGridded(0), itsSamplesDegridded(0),
    itsVectorsFlagged(0), itsVectorsWFlagged(0), itsNumberGridded(0), itsNumberDegridded(0),
    itsTimeCoordinates(0.0), itsTimeConvFunctions(0.0), itsTimeGridded(0.0),
    itsTimeDegridded(0.0), itsDopsf(false), itsDopcf(false),
    itsTrackWeightPerOversamplePlane(false),itsPARotation(false),itsSwapPols(false),
    its2dGrid(),itsVisPols(),itsPolConv(),itsImagePolFrameVis(),itsImagePolFrameNoise(),
    itsPolVector(),itsImageChan(-1),itsGridIndex(-1),itsClearGrid(false)
{
    ASKAPCHECK(overSample>0, "Oversampling must be greater than 0");
    ASKAPCHECK(support>=0, "Maximum support must be zero or greater");
    ASKAPCHECK(padding>0, "Padding factor must be greater than 0");
}

    /// @brief copy constructor
    /// @details it is required to decouple arrays between the input object
    /// and the copy.
    /// @param[in] other input object
VisGridder::VisGridder(const VisGridder &other) :
     IVisGridder(other),
     itsAxes(other.itsAxes), itsShape(other.itsShape),
     itsUVCellSize(other.itsUVCellSize.copy()),
     itsSumWeights(other.itsSumWeights.copy()),
     itsPaddingFactor(other.itsPaddingFactor),
     itsSupport(other.itsSupport), itsOverSample(other.itsOverSample),
     itsModelIsEmpty(other.itsModelIsEmpty),
     itsName(other.itsName), itsSamplesGridded(other.itsSamplesGridded),
     itsSamplesDegridded(other.itsSamplesDegridded),
     itsVectorsFlagged(other.itsVectorsFlagged),
     itsVectorsWFlagged(other.itsVectorsWFlagged),
     itsNumberGridded(other.itsNumberGridded),
     itsNumberDegridded(other.itsNumberDegridded), itsTimeCoordinates(other.itsTimeCoordinates),
     itsTimeConvFunctions(other.itsTimeConvFunctions),
     itsTimeGridded(other.itsTimeGridded),
     itsTimeDegridded(other.itsTimeDegridded),
     itsDopsf(other.itsDopsf),
     itsDopcf(other.itsDopcf),
     itsFreqMapper(other.itsFreqMapper),
     itsConvFuncOffsets(other.itsConvFuncOffsets),
     itsTrackWeightPerOversamplePlane(other.itsTrackWeightPerOversamplePlane),
     itsPARotation(other.itsPARotation), itsPARotAngle(other.itsPARotAngle),
     itsSwapPols(other.itsSwapPols),
     its2dGrid(other.its2dGrid.copy()),itsVisPols(other.itsVisPols.copy()),
     itsPolConv(other.itsPolConv),itsImagePolFrameVis(other.itsImagePolFrameVis.copy()),
     itsImagePolFrameNoise(other.itsImagePolFrameNoise.copy()),itsPolVector(other.itsPolVector.copy()),
     itsImageChan(other.itsImageChan),itsGridIndex(other.itsGridIndex),
     itsClearGrid(other.itsClearGrid)
{
   deepCopyOfSTDVector(other.itsConvFunc,itsConvFunc);
   deepCopyOfSTDVector(other.itsGrid, itsGrid);
   if(other.itsVisWeight) {
      itsVisWeight = other.itsVisWeight->clone();
   } else {
      itsVisWeight = other.itsVisWeight;
   }
}

/// @brief assignment operator
/// @details it is required to decouple arrays between the input object
/// and the copy.
/// @param[in] other input object
/// @return reference to itself
VisGridder& VisGridder::operator=(const VisGridder &)
{
  ASKAPTHROW(AskapError, "This method is not supposed to be called!");
  return *this;
}


VisGridder::~VisGridder() {

    ASKAP_LOGGER(logger, ".VisGridder.~VisGridder");
    if (itsNumberGridded>0) {
        if (isPSFGridder()) {
            ASKAPLOG_INFO_STR(logger, "VisGridder PSF gridding statistics");
            ASKAPLOG_INFO_STR(logger, "   PSF samples gridded       = "
                              << itsSamplesGridded);
            ASKAPLOG_INFO_STR(logger, "   Visibility vectors flagged (psf)     = "
                              << itsVectorsFlagged);
            ASKAPLOG_INFO_STR(logger, "   Visibility vectors with |w|>wmax (psf) = "
                              << itsVectorsWFlagged);
            ASKAPLOG_INFO_STR(logger, "   Total time for PSF gridding   = "
                << itsTimeGridded << " (s)");
            ASKAPLOG_INFO_STR(logger, "   PSF gridding time         = " << 1e6
                 *itsTimeGridded/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   Total time converting for PSF = "
                << itsTimeCoordinates << " (s)");
            ASKAPLOG_INFO_STR(logger, "   Total time building CFs and indices for PSF = "
                << itsTimeConvFunctions << " (s)");
            ASKAPLOG_INFO_STR(logger, "   PSF coord conversion      = "
                  << 1e6 * itsTimeCoordinates/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   PSF CFs and indices      = "
                  << 1e6 * itsTimeConvFunctions/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   Points gridded (psf)        = "
                  << itsNumberGridded);
            ASKAPLOG_INFO_STR(logger, "   Time per point (psf)        = " << 1e9
                  *itsTimeGridded/itsNumberGridded << " (ns)");
            ASKAPLOG_INFO_STR(logger, "   Performance for PSF         = "
                << 8.0 * 1e-9 * itsNumberGridded/itsTimeGridded << " GFlops");
        }
        else if (isPCFGridder()) {
            ASKAPLOG_INFO_STR(logger, "VisGridder PreConditioner Function gridding statistics");
            ASKAPLOG_INFO_STR(logger, "   PCF samples gridded       = "
                              << itsSamplesGridded);
            ASKAPLOG_INFO_STR(logger, "   Visibility vectors flagged (pcf)     = "
                              << itsVectorsFlagged);
            ASKAPLOG_INFO_STR(logger, "   Visibility vectors with |w|>wmax (pcf) = "
                              << itsVectorsWFlagged);
            ASKAPLOG_INFO_STR(logger, "   Total time for PCF gridding   = "
                << itsTimeGridded << " (s)");
            ASKAPLOG_INFO_STR(logger, "   PCF gridding time         = " << 1e6
                 *itsTimeGridded/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   Total time converting for PCF = "
                << itsTimeCoordinates << " (s)");
            ASKAPLOG_INFO_STR(logger, "   Total time building CFs and indices for PCF = "
                << itsTimeConvFunctions << " (s)");
            ASKAPLOG_INFO_STR(logger, "   PCF coord conversion      = "
                  << 1e6 * itsTimeCoordinates/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   PCF CFs and indices      = "
                  << 1e6 * itsTimeConvFunctions/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   Points gridded (pcf)        = "
                  << itsNumberGridded);
            ASKAPLOG_INFO_STR(logger, "   Time per point (pcf)        = " << 1e9
                  *itsTimeGridded/itsNumberGridded << " (ns)");
            ASKAPLOG_INFO_STR(logger, "   Performance for PCF         = "
                << 8.0 * 1e-9 * itsNumberGridded/itsTimeGridded << " GFlops");
        }
        else {
            ASKAPLOG_INFO_STR(logger, "VisGridder gridding statistics");
            ASKAPLOG_INFO_STR(logger, "   Samples gridded       = "
                         << itsSamplesGridded);
            ASKAPLOG_INFO_STR(logger, "   Visibility vectors flagged       = "
                          << itsVectorsFlagged);
             ASKAPLOG_INFO_STR(logger, "   Visibility vectors with |w|>wmax = "
                           << itsVectorsWFlagged);
            ASKAPLOG_INFO_STR(logger, "   Total time gridding   = "
                         << itsTimeGridded << " (s)");
            ASKAPLOG_INFO_STR(logger, "   Gridding time         = " << 1e6
                  *itsTimeGridded/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   Total time converting = "
                << itsTimeCoordinates << " (s)");
            ASKAPLOG_INFO_STR(logger, "   Total time building CFs and indices = "
                << itsTimeConvFunctions << " (s)");
            ASKAPLOG_INFO_STR(logger, "   Coord conversion      = "
                  << 1e6 * itsTimeCoordinates/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   CFs and indices      = "
                  << 1e6 * itsTimeConvFunctions/itsSamplesGridded << " (us) per sample");
            ASKAPLOG_INFO_STR(logger, "   Points gridded        = "
                << itsNumberGridded);
            ASKAPLOG_INFO_STR(logger, "   Time per point        = " << 1e9
                *itsTimeGridded/itsNumberGridded << " (ns)");
            ASKAPLOG_INFO_STR(logger, "   Performance           = "
                << 8.0 * 1e-9 * itsNumberGridded/itsTimeGridded << " GFlops");
        }
    }
    if (itsNumberDegridded>0) {
        ASKAPLOG_INFO_STR(logger, "VisGridder degridding statistics");
        ASKAPLOG_INFO_STR(logger, "   Samples degridded     = "
                << itsSamplesDegridded);
        ASKAPLOG_INFO_STR(logger, "   Total time degridding = "
                << itsTimeDegridded << " (s)");
        ASKAPLOG_INFO_STR(logger, "   Degridding time       = " << 1e6
                *itsTimeDegridded/itsSamplesDegridded << " (us) per sample");
        ASKAPLOG_INFO_STR(logger, "   Total time converting = "
                << itsTimeCoordinates << " (s)");
        ASKAPLOG_INFO_STR(logger, "   Total time building CFs and indices = "
                << itsTimeConvFunctions << " (s)");
        ASKAPLOG_INFO_STR(logger, "   Coord conversion      = "
                  << 1e6 * itsTimeCoordinates/itsSamplesDegridded << " (us) per sample");
        ASKAPLOG_INFO_STR(logger, "   CFs and indices      = "
                  << 1e6 * itsTimeConvFunctions/itsSamplesDegridded << " (us) per sample");
        ASKAPLOG_INFO_STR(logger, "   Points degridded      = "
                << itsNumberDegridded);
        ASKAPLOG_INFO_STR(logger, "   Time per point        = " << 1e9
                *itsTimeDegridded/itsNumberDegridded << " (ns)");
        ASKAPLOG_INFO_STR(logger, "   Performance           = "
                << 8.0 * 1e-9 * itsNumberDegridded/itsTimeDegridded << " GFlops");
    }
    if(unusedGridder()) {
      ASKAPLOG_WARN_STR(logger, "Unused gridder");
    }
    else {
      ASKAPLOG_INFO_STR(logger, "   Padding factor    = " << paddingFactor());
      if (itsTrackWeightPerOversamplePlane) {
          ASKAPLOG_INFO_STR(logger, "   Weights were tracked per oversampling plane");
      } else {
          ASKAPLOG_INFO_STR(logger, "   First oversampling plane was used to compute weights");
      }
      logUnusedSpectralPlanes();
      logCFCacheStats();
      if(tableName() != "") {
        save(tableName());
      }
    }
}

void VisGridder::save(const std::string& name) {
    ASKAP_LOGGER(logger, ".VisGridder.save");
    if (name.find("image:") != 0) {
        askap::scimath::ParamsCasaTable iptable(name, false);
        askap::scimath::Params ip;
        ASKAPLOG_DEBUG_STR(logger, "Saving " << itsConvFunc.size() << " entries in convolution function");
        for (unsigned int i=0; i<itsConvFunc.size(); i++) {

            casacore::Array<float> realC=real(itsConvFunc[i]);
            // ASKAPLOG_DEBUG_STR(logger, "Entry[" <<  i <<  "] has shape " <<  itsConvFunc[i].shape());
            std::ostringstream os;
            os<<"Real.Convolution";
            os.width(5);
            os.fill('0');
            os<<i;
            ip.add(os.str(), realC);
        }
        iptable.setParameters(ip);
    } else {
        if (itsNumberGridded == 0) {
            ASKAPLOG_DEBUG_STR(logger, "Ignore tablename="<<name<<
                               " option as no visibilities were gridded");
            return;
        }
        if (isPSFGridder()) {
            ASKAPLOG_DEBUG_STR(logger, "Ignore tablename="<<name<<
                               " option for the PSF gridder");
             return;
        } else if (isPCFGridder()) {
            ASKAPLOG_DEBUG_STR(logger, "Ignore tablename="<<name<<
                               " option for the Preconditioner Function gridder");
             return;
        }

        const std::string imgName = name.substr(6);
        // number of planes before oversampling
        const unsigned long nPlanes = itsConvFunc.size()/itsOverSample/itsOverSample;
        if (nPlanes > 0) {
            ASKAPLOG_DEBUG_STR(logger, "Saving convolution functions into a cube "<<
                                  imgName<<" with " << nPlanes<<
                                  " planes (first oversampling plane only)");
            ASKAPDEBUGASSERT(itsConvFunc.size()>0);
            int support = -1;
            for (unsigned int plane = 0; plane<nPlanes; ++plane) {
                 const int x = int(itsConvFunc[plane*itsOverSample*itsOverSample].nrow());
                 const int y = int(itsConvFunc[plane*itsOverSample*itsOverSample].ncolumn());
                 if (support < x) {
                     support = x;
                 }
                 if (support < y) {
                     support = y;
                 }
            }
            casacore::Cube<casacore::Float> imgBuffer(support, support, nPlanes);
            imgBuffer.set(0.);
            for (unsigned int plane = 0; plane<nPlanes; ++plane) {
                //unsigned int peakX = 0, peakY = 0;
                casacore::Float peakVal = -1.;
                for (int x = 0; x<int(imgBuffer.nrow()); ++x) {
                     for (int y = 0; y<int(imgBuffer.ncolumn()); ++y) {
                          const casacore::Matrix<casacore::Complex> thisCF = itsConvFunc[plane*itsOverSample*itsOverSample];
                          const int xOff = (support - int(thisCF.nrow()))/2;
                          const int yOff = (support - int(thisCF.ncolumn()))/2;
                          ASKAPDEBUGASSERT((xOff >= 0) && (yOff >= 0));
                          if ( (x - xOff >= int(thisCF.nrow())) || (y - yOff >= int(thisCF.ncolumn()))) {
                               continue;
                          }
                          if ( (x - xOff < 0) || (y - yOff < 0) ) {
                               continue;
                          }
                          imgBuffer(x,y,plane) = casacore::abs(thisCF(x - xOff,y - yOff));
                     }
                }
            }
            scimath::saveAsCasaImage(imgName,imgBuffer);
        }
    }
}

/// @brief helper method to print CF cache stats in the log
/// @details This method is largely intended for debugging. It writes down
/// to the log the support sizes/offsets for all convolution functions in the cache and
/// summarises the memory taken up by this cache (per gridder).
void VisGridder::logCFCacheStats() const
{
    ASKAP_LOGGER(logger, ".VisGridder.logCFCacheStats");
   // number of planes before oversampling
   ASKAPDEBUGASSERT(itsOverSample>0);
   const unsigned long nPlanes = itsConvFunc.size()/itsOverSample/itsOverSample;
   unsigned long memUsed = 0;
   for (unsigned int plane = 0; plane<nPlanes; ++plane) {
       ASKAPDEBUGASSERT(plane*itsOverSample*itsOverSample < itsConvFunc.size());
       const casacore::IPosition shape = itsConvFunc[plane*itsOverSample*itsOverSample].shape();
       if (shape.nelements() < 2) {
           ASKAPLOG_DEBUG_STR(logger, "CF plane="<<plane<<" (before oversampling) is malformed");
           continue;
       }
       if (shape.product() == 0) {
           ASKAPLOG_DEBUG_STR(logger, "CF plane="<<plane<<" (before oversampling) is unused");
           memUsed += sizeof(casacore::Matrix<casacore::Complex>);
           continue;
       }
       if ((shape[0] != shape[1]) || (shape[0] % 2 != 1)) {
           ASKAPLOG_DEBUG_STR(logger, "CF plane="<<plane<<
                                      " (before oversampling) has a rectangular support or even size");
           memUsed += sizeof(casacore::Matrix<casacore::Complex>) +
                      sizeof(casacore::Complex)*shape.product()*itsOverSample*itsOverSample;
           continue;
       }
       const int support = (shape[0] - 1) / 2;
       const std::pair<int,int> cfOffset = getConvFuncOffset(plane);
       ASKAPLOG_DEBUG_STR(logger, "CF plane="<<plane<<" (before oversampling): support="<<
                                  support<<", size="<<shape[0]<<
                                  " at offset ("<<cfOffset.first<<","<<cfOffset.second<<")");
       memUsed += sizeof(casacore::Matrix<casacore::Complex>) +
                  sizeof(casacore::Complex)*shape[0]*shape[1]*itsOverSample*itsOverSample;
   }
   if (nPlanes > 0) {
       float effectiveSize = (float(memUsed)-float(sizeof(casacore::Matrix<casacore::Complex>)*nPlanes)) /
                             (sizeof(casacore::Complex)*itsOverSample*itsOverSample*nPlanes);
       ASKAPLOG_DEBUG_STR(logger, "Cache of convolution functions take " <<
                                  float(memUsed)/1024/1024<<" Mb of memory or ");
       ASKAPLOG_DEBUG_STR(logger, float(memUsed)/nPlanes/1024/1024 <<
                                  " Mb of memory per plane (before oversampling)");
       ASKAPDEBUGASSERT(effectiveSize>=0.);
       effectiveSize = sqrt(effectiveSize);
       ASKAPLOG_DEBUG_STR(logger, "Effective CF size (in terms of memory usage) is "<<
                                  long(effectiveSize)<<", effective support="<<
                                  long((effectiveSize-1)/2));
   }
}

// DAM @TODO this really should have a new name. initGriddingInfo or initGriddingData perhaps

/// Initialise grid and convolution function kernel index caches used in this routine
void VisGridder::initGridIndices(const boost::shared_ptr<VisInfo>& visInfo,
                                 boost::shared_ptr<GriddingInfo>& griddingInfo) {

    ASKAPDEBUGTRACE("VisGridder::initGridIndices");

    ASKAP_LOGGER(logger, ".VisGridder.initGridIndices");

    const uint nSamples = visInfo->nSample();
    ASKAPDEBUGASSERT(nSamples == visInfo->sample().size());

    if (!griddingInfo->kernelIndex().empty()) {
        ASKAPLOG_INFO_STR(logger, "Using kernel and grid index caches.");
        ASKAPDEBUGASSERT(griddingInfo->nSample() == visInfo->nSample());
        ASKAPDEBUGASSERT(griddingInfo->nPol() == visInfo->nPol());
        ASKAPDEBUGASSERT(griddingInfo->nChannel() == visInfo->nChannel());
        ASKAPDEBUGASSERT(!griddingInfo->gridIndexU().empty());
        ASKAPDEBUGASSERT(!griddingInfo->gridIndexV().empty());
        ASKAPDEBUGASSERT(griddingInfo->gridIndexU().size() == nSamples);
        ASKAPDEBUGASSERT(griddingInfo->gridIndexV().size() == nSamples);
        ASKAPDEBUGASSERT(griddingInfo->kernelIndex().size() == nSamples);
        ASKAPDEBUGASSERT(griddingInfo->stokes() == visInfo->nPol());
        ASKAPDEBUGASSERT(griddingInfo->frequencyList() == visInfo->nChannel());
        ASKAPDEBUGASSERT(griddingInfo->phasor().size() == nSamples);
        ASKAPDEBUGASSERT(griddingInfo->chan().size() == nSamples);
        ASKAPDEBUGASSERT(griddingInfo->weight().size() == nSamples);
        ASKAPDEBUGASSERT(griddingInfo->sample().size() == nSamples);
        ASKAPDEBUGASSERT(griddingInfo->model().size() == nSamples);
        return;
    }

    ASKAPLOG_INFO_STR(logger, "Setting kernel and grid index caches.");

    // copy over vector dimensions
    griddingInfo->nSample(visInfo->nSample());
    griddingInfo->nPol(visInfo->nPol());
    griddingInfo->nChannel(visInfo->nChannel());
    // copy over vectors that will be needed during gridding but that not set below
    griddingInfo->stokes()        = visInfo->stokes();
    griddingInfo->frequencyList() = visInfo->frequencyList();
    griddingInfo->phasor()        = visInfo->phasor();
    griddingInfo->chan()          = visInfo->chan();
    griddingInfo->weight()        = visInfo->weight();
    griddingInfo->sample()        = visInfo->sample();

    // the model vis aren't part of visInfo, so set a vector of zeros here
    std::vector<std::vector<std::complex<float> > > &model = griddingInfo->model();
    model.resize(nSamples);
    for (uint i=0; i<nSamples; ++i) {
        model[i].resize(griddingInfo->nPol(), 0.0);
    }

    // dereference any vectors used in the loop
    const std::vector<double> &u = visInfo->u();
    ASKAPDEBUGASSERT(u.size() == nSamples);
    const std::vector<double> &v = visInfo->v();
    ASKAPDEBUGASSERT(v.size() == nSamples);

    const std::vector<double> &frequencyList = visInfo->frequencyList();
    ASKAPASSERT(frequencyList.size() == visInfo->nChannel());

    std::vector<int> &gridIndexU = griddingInfo->gridIndexU();
    std::vector<int> &gridIndexV = griddingInfo->gridIndexV();
    std::vector<int> &kernelIndex = griddingInfo->kernelIndex();
    std::vector<int> &griddingFlag = griddingInfo->flag();

    // resize new caches
    gridIndexU.resize(nSamples);
    gridIndexV.resize(nSamples);
    kernelIndex.resize(nSamples);
    griddingFlag.resize(nSamples);
 
    ASKAPCHECK(itsUVCellSize.size()==2, "UV cell sizes not yet set");

    // also flag any data that don't map to an output frequency plane
    //std::vector<bool> &flag = visInfo->flag();
    const std::vector<int> &flag = visInfo->flag();
    ASKAPDEBUGASSERT(flag.size() == nSamples);
    const std::vector<int> &chan = visInfo->chan();
    ASKAPDEBUGASSERT(chan.size() == nSamples);

    //ASKAPDEBUGASSERT(frequencyList.size() > 0);
    ASKAPASSERT(frequencyList.size() > 0);
    itsFreqMapper.setupMapping(frequencyList);
 
    for (uint i=0; i<nSamples; ++i) {

        // @todo DAM clean up itsVectorsWFlagged
        // check if w is in range
        const int wPlaneIndex = cIndex(i);
        if (wPlaneIndex<0) {
            itsVectorsWFlagged+=1;
            //griddingFlag[i] = true;
            griddingFlag[i] = 1;
        }
        const int gInd = gIndex(i);
        if ((gInd<0) || (gInd>=int(itsGrid.size()))) {
            // can't use this data - w out of range
            itsVectorsWFlagged +=1;
            //griddingFlag[i] = true;
            griddingFlag[i] = 1;
        }
        if (!itsFreqMapper.isMapped(chan[i])) {
            //griddingFlag[i] = true;
            griddingFlag[i] = 1;
        }
        // check if sample is flagged
        if (flag[i] || griddingFlag[i]) {
            //griddingFlag[i] = true;
            griddingFlag[i] = 1;
            itsVectorsFlagged+=1;
            continue;
        }

        /// Scale U,V to integer pixels plus fractional terms
        const double uScaled = u[i]/itsUVCellSize(0);
        int iu = askap::nint(uScaled);
        int fracu=askap::nint(itsOverSample*(double(iu)-uScaled));
        if (fracu<0) {
            iu+=1;
            fracu += itsOverSample;
        } else if (fracu>=itsOverSample) {
            iu-=1;
            fracu -= itsOverSample;
        }
        ASKAPCHECK(fracu>-1,
                "Fractional offset in u is negative, uScaled="<<uScaled<<
                " iu="<<iu<<" oversample="<<itsOverSample<<" fracu="<<fracu);
        ASKAPCHECK(fracu<itsOverSample,
                "Fractional offset in u exceeds oversampling, uScaled="<<uScaled<<
                " iu="<<iu<<" oversample="<<itsOverSample<<" fracu="<<fracu);
        gridIndexU[i] = iu + itsShape(0)/2;
 
        const double vScaled = v[i]/itsUVCellSize(1);
        int iv = askap::nint(vScaled);
        int fracv=askap::nint(itsOverSample*(double(iv)-vScaled));
        if (fracv<0) {
            iv+=1;
            fracv += itsOverSample;
        } else if (fracv>=itsOverSample) {
            iv-=1;
            fracv -= itsOverSample;
        }
        ASKAPCHECK(fracv>-1,
                "Fractional offset in v is negative, vScaled="<<vScaled<<
                " iv="<<iv<<" oversample="<<itsOverSample<<" fracv="<<fracv);
        ASKAPCHECK(fracv<itsOverSample,
                "Fractional offset in v exceeds oversampling, vScaled="<<vScaled<<
                " iv="<<iv<<" oversample="<<itsOverSample<<" fracv="<<fracv);
        gridIndexV[i] = iv + itsShape(1)/2;

        // Account for a possible offset of the convolution function
        // Is the right thing to do for the uv sampling function used to generate robust weighting?
        //  - It has to be for the Wiener filter, so I suppose so traditional weighting.
        //  - An offset beam's samples will smear out as a function of w, but should they be weighted together?
        const std::pair<int,int> cfOffset = getConvFuncOffset(wPlaneIndex);
        gridIndexU[i] += cfOffset.first;
        gridIndexV[i] += cfOffset.second;

        /// Check if this point lies on the grid (taking into account the support)
        const int maxsupport = (int(itsConvFunc.back().nrow()) - 1) / 2;
        if ( (gridIndexU[i]-maxsupport <= 0) || (gridIndexU[i]+maxsupport >= itsShape(0)) ||
             (gridIndexV[i]-maxsupport <= 0) || (gridIndexV[i]+maxsupport >= itsShape(1)) ) {
            //griddingFlag[i] = true;
            griddingFlag[i] = 1;
            itsVectorsFlagged+=1;
            continue;
        }

        kernelIndex[i] = fracu + itsOverSample*(fracv + itsOverSample*wPlaneIndex);

        ASKAPCHECK(kernelIndex[i]>-1,"Index into convolution functions is less than zero");
        ASKAPCHECK(kernelIndex[i]<int(itsConvFunc.size()), "Index into convolution functions exceeds number of planes");

    } //end of i loop

}

/// This is a generic grid/degrid
void VisGridder::generic(boost::shared_ptr<GriddingInfo>& griddingInfo, bool forward) {

    ASKAPDEBUGTRACE("VisGridder::generic");
    ASKAP_LOGGER(logger, ".VisGridder.generic");

    if (forward && itsModelIsEmpty) {
        return;
    }
    if (forward && isPSFGridder()) {
        ASKAPTHROW(AskapError,
            "Logic error: the gridder is not supposed to be used for degridding in the PSF mode")
    }
    if (forward && isPCFGridder()) {
        ASKAPTHROW(AskapError,
            "Logic error: the gridder is not supposed to be used for degridding in the PCF mode")
    }

    if (!forward) {
       ASKAPCHECK(itsSumWeights.nelements()>0, "SumWeights not yet initialised");
    }

    // Timer
    casacore::Timer timer;
    timer.mark();

    ASKAPCHECK(itsSupport>=0, "Support must be zero or greater");
    ASKAPCHECK(itsUVCellSize.size()==2, "UV cell sizes not yet set");

    // dereference anything used in the big loops

    const uint nSamples = griddingInfo->nSample();
    const uint nPol = griddingInfo->nPol();
    const uint nChan = griddingInfo->nChannel();

    // polarisation lists
    const casacore::Vector<casacore::Stokes::StokesTypes> &stokes = griddingInfo->stokes();

    ASKAPDEBUGASSERT(stokes.size() == nPol);

    // frequency lists
    const std::vector<double> &frequencyList = griddingInfo->frequencyList();
    itsFreqMapper.setupMapping(frequencyList);
    ASKAPDEBUGASSERT(frequencyList.size() == nChan);

    // coordinate index buffers
    const std::vector<int> &gridIndexU = griddingInfo->gridIndexU();
    const std::vector<int> &gridIndexV = griddingInfo->gridIndexV();
    const std::vector<int> &kernelIndex = griddingInfo->kernelIndex();
    ASKAPDEBUGASSERT(gridIndexU.size() == nSamples);
    ASKAPDEBUGASSERT(gridIndexV.size() == nSamples);
    ASKAPDEBUGASSERT(kernelIndex.size() == nSamples);

    // other buffers
    const std::vector<std::complex<float> > &phasor = griddingInfo->phasor();
    const std::vector<int> &chan = griddingInfo->chan();
    //const std::vector<bool> &flag = griddingInfo->flag();
    const std::vector<int> &flag = griddingInfo->flag();
    const std::vector<std::vector<float> > &weight = griddingInfo->weight();
    const std::vector<std::vector<std::complex<float> > > &sample = griddingInfo->sample();
    std::vector<std::vector<std::complex<float> > > &model = griddingInfo->model();
    ASKAPDEBUGASSERT(phasor.size() == nSamples);
    ASKAPDEBUGASSERT(chan.size() == nSamples);
    ASKAPDEBUGASSERT(flag.size() == nSamples);
    ASKAPDEBUGASSERT(weight.size() == nSamples);
    ASKAPDEBUGASSERT(sample.size() == nSamples);
    ASKAPDEBUGASSERT(model.size() == nSamples);

    ASKAPDEBUGASSERT(itsShape.nelements()>=2);
    casacore::IPosition ipStart(4, 0, 0, 0, 0);
    const casacore::IPosition onePlane(2,shape()(0),shape()(1));
    if (its2dGrid.shape()(0)!=onePlane(0) || its2dGrid.shape()(1)!=onePlane(1)) {
        // Initialise the 2d reference
        its2dGrid.resize(onePlane);
    }
    // number of polarisation planes in the grid
    const casacore::uInt nImagePols = (shape().nelements()<=2) ? 1 : shape()[2];
    ASKAPCHECK(nImagePols == nPol, "Inconsistent polarisations. Has buffer been converted?");

    // moving these checks out of major loop, assuming that itsConvFunc.size() < nSamples
    for (uint cInd=0; cInd<itsConvFunc.size(); ++cInd) {
        casacore::Matrix<casacore::Complex> & convFunc(itsConvFunc[cInd]);
        // support only square convolution functions
        ASKAPDEBUGASSERT(convFunc.nrow() == convFunc.ncolumn());
        // support only odd-sided convolution functions
        ASKAPCHECK(convFunc.nrow() % 2 == 1, "Expect odd-sided convolution functions: "<<convFunc.nrow());
        //const int support = (int(convFunc.nrow()) - 1) / 2;
        ASKAPCHECK(convFunc.nrow() >= 1, "Support must be zero or greater, kernel width = "<<convFunc.nrow());
    }

    // moving these checks out of major loop, assuming that itsConvFunc.size() < nSamples
    if (!forward) {
        ASKAPCHECK(itsSumWeights.nelements()>0, "Sum of weights not yet initialised");
        ASKAPDEBUGASSERT(itsSumWeights.shape().nelements() >= 3);
        ASKAPDEBUGASSERT(nPol <= uint(itsSumWeights.shape()(1)));
        for (uint ch=0; ch<nChan; ++ch) {
            if (!itsFreqMapper.isMapped(ch)) continue; // these data already flagged
            ASKAPDEBUGASSERT(itsFreqMapper(ch) < int(itsSumWeights.shape()(2)));
        }
    }

    // moving MFS weight calcs out of loop as well
    std::vector<float> MFSWeight(nChan, 1.0);
    if (itsVisWeight) {
        // itsVisWeight class has been initialised, so pre-calc weights
        //  - note 1: assuming it has been done for MFS weighting using VisWeightsMultiFrequency.
        //  - note 2: other weighting types are possible, though. Might be safer to use a new class here.
        //  - note 3: given note 1, the other two getWeight parameters will be ignored, so use zeros.
        for (uint ch=0; ch<nChan; ++ch) {
            MFSWeight[ch] = itsVisWeight->getWeight(0,frequencyList[ch],0);
        }
    }

    if (forward) {

        // Loop over all samples adding them to the grid
        for (uint i=0; i<nSamples; ++i) {
 
            if (flag[i]) continue;
 
            // obtain which channel of the image this sample is mapped to. Unmapped channels have been flagged.
            const int imageChan = itsFreqMapper(chan[i]);
            ipStart(3) = imageChan;
 
            const int gInd = gIndex(i);
 
            casacore::Matrix<casacore::Complex> & convFunc(itsConvFunc[kernelIndex[i]]);
            const int support = (int(convFunc.nrow()) - 1) / 2;
 
            const float mfsWt = MFSWeight[chan[i]];
 
            // Now loop over all image polarizations
            /// @todo swap order with sample loop and do grid referencing outside big loops
            /// @todo could also have an outer loop over Taylor terms, unless they are done in parallel
            for (uint pol=0; pol<nImagePols; ++pol) {
 
                // This seems to be the quickest way to get a reference to the matrix we want
                // It assumes itsGrid is contiguous
                ASKAPDEBUGASSERT(itsGrid[gInd].contiguousStorage());
                ipStart(2) = pol;
                // Check if we need to update the grid reference
                if ( nImagePols>1 || imageChan!=itsImageChan || gInd!=itsGridIndex ) {
                    its2dGrid.takeStorage(onePlane,&itsGrid[gInd](ipStart),casacore::SHARE);
                    itsImageChan = imageChan;
                    itsGridIndex = gInd;
                }
 
                casacore::Complex cVis(0.,0.);
                degridKernel(cVis, convFunc, its2dGrid, gridIndexU[i], gridIndexV[i], support);
                itsSamplesDegridded+=1.0;
                itsNumberDegridded+=double((2*support+1)*(2*support+1));
                if (itsVisWeight) {
                    cVis *= mfsWt;
                }
                model[i][pol] = cVis*phasor[i];
 
            }
 
        } //end of i loop

        itsTimeDegridded+=timer.real();

    } else {

        // Loop over all samples adding them to the grid
        for (uint i=0; i<nSamples; ++i) {
 
            if (flag[i]) continue;
 
            // obtain which channel of the image this sample is mapped to. Unmapped channels have been flagged.
            const int imageChan = itsFreqMapper(chan[i]);
            ipStart(3) = imageChan;
 
            const int gInd = gIndex(i);
 
            casacore::Matrix<casacore::Complex> & convFunc(itsConvFunc[kernelIndex[i]]);
            const int support = (int(convFunc.nrow()) - 1) / 2;
 
            const float mfsWt = MFSWeight[chan[i]];
 
            // Now loop over all image polarizations
            /// @todo swap order with sample loop and do grid referencing outside big loops
            for (uint pol=0; pol<nImagePols; ++pol) {
 
                // This seems to be the quickest way to get a reference to the matrix we want
                // It assumes itsGrid is contiguous
                ASKAPDEBUGASSERT(itsGrid[gInd].contiguousStorage());
                ipStart(2) = pol;
                // Check if we need to update the grid reference
                if ( nImagePols>1 || imageChan!=itsImageChan || gInd!=itsGridIndex ) {
                    its2dGrid.takeStorage(onePlane,&itsGrid[gInd](ipStart),casacore::SHARE);
                    itsImageChan = imageChan;
                    itsGridIndex = gInd;
                }
 
                const float visWt = weight[i][pol];

                // store accumulated weights
                const int sumWeightsRow = SumOfWeightsRowFromCfIndex(kernelIndex[i]);
                ASKAPDEBUGASSERT(sumWeightsRow < int(itsSumWeights.shape()(0)));
                itsSumWeights(sumWeightsRow, pol, imageChan) += visWt;
                if (!isPSFGridder() && !isPCFGridder()) {
                    /// Gridding visibility data onto grid
                    casacore::Complex rVis = phasor[i] * conj(sample[i][pol]) * visWt * mfsWt;
                    gridKernel(its2dGrid, convFunc, rVis, gridIndexU[i], gridIndexV[i], support);
                    itsSamplesGridded+=1.0;
                    itsNumberGridded+=double((2*support+1)*(2*support+1));
                }
                /// Grid the PSF?
                if (isPSFGridder()) {
                    casacore::Complex uVis(visWt * mfsWt,0.);
                    gridKernel(its2dGrid, convFunc, uVis, gridIndexU[i], gridIndexV[i], support);
                    itsSamplesGridded+=1.0;
                    itsNumberGridded+=double((2*support+1)*(2*support+1));
 
                }
                /// Grid the preconditioner function?
                if (isPCFGridder()) {
                    // We don't want different preconditioning for different Taylor terms.
                    casacore::Complex uVis(visWt,0.);
                    // Storing w info in the imaginary part of the PCF, so make them add with conjugate symmetry.
                    //  - conjugate based on the sign of v. If v==0, base it on the sign of u.
                    if ((gridIndexV[i]<itsShape(1)/2 && gridIndexU[i]>=itsShape(0)/2) ||
                        (gridIndexV[i]<=itsShape(1)/2 && gridIndexU[i]<itsShape(0)/2)) {
                      casacore::Matrix<casacore::Complex> conjFunc = conj(convFunc);
                      gridKernel(its2dGrid, conjFunc, uVis, gridIndexU[i], gridIndexV[i], support);
                    } else {
                      gridKernel(its2dGrid, convFunc, uVis, gridIndexU[i], gridIndexV[i], support);
                    }
                    itsSamplesGridded+=1.0;
                    itsNumberGridded+=double((2*support+1)*(2*support+1));
                }
 
            }
 
        } //end of i loop

        itsTimeGridded+=timer.real();

    }

}

/// @brief Grid the visibility data.
/// @param griddingInfo GriddingInfo object to work with
/// @note a non-const adapter is created behind the scene. If no on-the-fly visibility
/// correction is performed, this adapter is equivalent to the original const data accessor
void VisGridder::grid(boost::shared_ptr<GriddingInfo>& griddingInfo) {
    ASKAPTRACE("VisGridder::grid");
    generic(griddingInfo, false);
}

/// @brief Degrid the visibility data.
/// @param[in] griddingInfo GriddingInfo object to work with
void VisGridder::degrid(boost::shared_ptr<GriddingInfo>& griddingInfo) {
    ASKAPTRACE("VisGridder::degrid");
    generic(griddingInfo, true);
}

void VisGridder::gridKernel(casacore::Matrix<casacore::Complex>& grid,
                            casacore::Matrix<casacore::Complex>& convFunc, const casacore::Complex& cVis,
                            const int iu, const int iv, const int support) {
    casacore::Float rVis = cVis.real();
    casacore::Float iVis = cVis.imag();
    for (int suppv = -support; suppv <= support; suppv++) {
        // point to the start of the current v column
        const int voff = suppv + support;
        const int uoff = 0;
        // Writing the multiply in real/imag is twice as fast with gcc
        casacore::Float *wtPtrF = reinterpret_cast<casacore::Float *> (&convFunc(uoff, voff));
        casacore::Float *gridPtrF = reinterpret_cast<casacore::Float *> (&grid(iu - support, iv + suppv));
        for (int suppu = -support; suppu <= support; suppu++, wtPtrF+=2, gridPtrF+=2) {
            gridPtrF[0] += rVis * wtPtrF[0] - iVis * wtPtrF[1];
            gridPtrF[1] += rVis * wtPtrF[1] + iVis * wtPtrF[0];
        }
    }
    //for (int suppv = -support; suppv <= support; suppv++) {
    //    const int voff = suppv + support;
    //    for (int suppu = -support; suppu <= support; suppu++) {
    //        const int uoff = suppu + support;
    //        casacore::Complex wt = convFunc(uoff, voff);
    //        grid(iu+suppu, iv+suppv) += cVis * wt;
    //    }
    //}
}

void VisGridder::degridKernel(casacore::Complex& cVis,
        const casacore::Matrix<casacore::Complex>& convFunc,
        const casacore::Matrix<casacore::Complex>& grid,
        const int iu, const int iv, const int support) {
    /// Degridding from grid to visibility. Here we just take a weighted sum of the visibility
    /// data using the convolution function as the weighting function.
    cVis = 0.0;
    for (int suppv = -support; suppv <= support; suppv++) {
        // point to the start of the current v column
        const int voff = suppv + support;
        const int uoff = 0;
        const casacore::Complex *wtPtr = &convFunc(uoff, voff);
        const casacore::Complex *gridPtr = &(grid(iu - support, iv + suppv));
        // Writing the multiply in real/imag is twice as fast with gcc
        // Doing the 'reinterpret_cast' thing to avoid complex like in grid, doesn't help here.
        for (int suppu = -support; suppu <= support; suppu++, wtPtr++, gridPtr++) {
            cVis += casacore::Complex( (*wtPtr).real()*(*gridPtr).real()+(*wtPtr).imag()*(*gridPtr).imag(),
                                      -(*wtPtr).real()*(*gridPtr).imag()+(*wtPtr).imag()*(*gridPtr).real());
        }
    }
    //for (int suppv = -support; suppv <= support; suppv++) {
    //    const int voff = suppv + support;
    //    for (int suppu = -support; suppu <= support; suppu++) {
    //        const int uoff = suppu + support;
    //        casacore::Complex wt = convFunc(uoff, voff);
    //        cVis += wt * conj(grid(iu+suppu, iv+suppv));
    //    }
    //}
}

/// @brief obtain the centre of the image
/// @details This method extracts RA and DEC axes from itsAxes and
/// forms a direction measure corresponding to the middle of each axis.
/// @return direction measure corresponding to the image centre
casacore::MVDirection VisGridder::getImageCentre() const
{
   ASKAPCHECK(itsAxes.hasDirection(),"Direction axis is missing. axes="<<itsAxes);
   casacore::MDirection out;
   casacore::Vector<casacore::Double> centrePixel(2);
   ASKAPDEBUGASSERT(itsShape.nelements()>=2);
   ASKAPDEBUGASSERT(paddingFactor()>0);
   for (size_t dim=0; dim<2; ++dim) {
        centrePixel[dim] = double(itsShape[dim])/2./double(paddingFactor());
   }
   ASKAPCHECK(syncHelper.toWorld(itsAxes.directionAxis(), out, centrePixel),
        "Unable to obtain world coordinates for the centre of the image. Something is wrong with the coordinate system");
   return out.getValue();
}

/// @brief obtain the tangent point
/// @details For faceting all images should be constructed for the same tangent
/// point. This method extracts the tangent point (reference position) from the
/// coordinate system.
/// @return direction measure corresponding to the tangent point
casacore::MVDirection VisGridder::getTangentPoint() const
{
   ASKAPCHECK(itsAxes.hasDirection(),"Direction axis is missing. axes="<<itsAxes);
   const casacore::Vector<casacore::Double> refVal(itsAxes.directionAxis().referenceValue());
   ASKAPDEBUGASSERT(refVal.nelements() == 2);
   const casacore::Quantum<double> refLon(refVal[0], "rad");
   const casacore::Quantum<double> refLat(refVal[1], "rad");
   const casacore::MVDirection out(refLon, refLat);
   return out;
}

/// @brief Conversion helper function
/// @details Copies in to out expanding double into complex values and
/// padding appropriately if necessary (itsPaddingFactor is more than 1)
/// @param[out] out complex output array
/// @param[in] in double input array
/// @param[in] padding padding factor
void VisGridder::toComplex(casacore::Array<imtypeComplex>& out,
        const casacore::Array<imtype>& in, const float padding) {
    ASKAPDEBUGTRACE("VisGridder::toImtypeComplex");

    out.resize(scimath::PaddingUtils::paddedShape(in.shape(),padding));
    out.set(0.);
    casacore::Array<imtypeComplex> subImage = scimath::PaddingUtils::extract(out,padding);
    casacore::convertArray<imtypeComplex, imtype>(subImage, in);
}

/// @brief Conversion helper function
/// @details Copies real part of in into imtype array and
/// extracting an inner rectangle if necessary (itsPaddingFactor is more than 1)
/// @param[out] out real output array
/// @param[in] in complex input array
/// @param[in] padding padding factor
void VisGridder::toDouble(casacore::Array<imtype>& out,
        const casacore::Array<imtypeComplex>& in, const float padding) {
  ASKAPDEBUGTRACE("VisGridder::toDouble");
  casacore::Array<imtypeComplex> wrapper(in);
  const casacore::Array<imtypeComplex> subImage = scimath::PaddingUtils::extract(wrapper,padding);
  out.resize(subImage.shape());
  out = real(subImage);
}

/// @brief set up itsStokes using the information from itsAxes and itsShape
void VisGridder::initStokes()
{
   const int nPol = itsShape.nelements()>=3 ? itsShape[2] : 1;
   if (itsAxes.has("STOKES")) {
       itsStokes = itsAxes.stokesAxis();
   } else {
       itsStokes.resize(1);
       itsStokes[0] = casacore::Stokes::I;
   }
   ASKAPCHECK(int(itsStokes.nelements()) == nPol, "Stokes axis is not consistent with the shape of the grid. There are "<<
              nPol<<" planes in the grid and "<<itsStokes.nelements()<<
              " polarisation descriptors defined by the STOKES axis");
}

void VisGridder::initialiseCommon(const VisData::ShPtr& visData) {
    ASKAPTRACE("VisGridder::initialiseCommon");

    ASKAPDEBUGASSERT(visData->shape().nelements()>=2);
    itsShape = scimath::PaddingUtils::paddedShape(visData->shape(),paddingFactor());

    initialiseCellSize(visData->axes());
    initStokes();
    initialiseFreqMapping();

    /// We only need one grid
    itsGrid.resize(1);
    itsGrid[0].resize(itsShape);
    itsGrid[0].set(0.0);

    // This ensures we reinitalise the pol converter
    itsVisPols.resize(0);

    for (uint k = 0; k<visData->nPartitions(); ++k) {
        initIndices(visData->visInfo(k));
        initConvolutionFunction(visData->visInfo(k));
        initGridIndices(visData->visInfo(k), visData->griddingInfo(k));
        // add in the convolution kernel cache
        visData->griddingInfo(k)->setConvFunc(itsConvFunc);
        visData->griddingInfo(k)->setConvFuncOffsets(itsConvFuncOffsets);
    }

}

void VisGridder::initialiseGrid(const VisData::ShPtr& visData, const bool dopsf, const bool dopcf) {
    ASKAPTRACE("VisGridder::initialiseGrid");
    ASKAP_LOGGER(logger, ".VisGridder.initialiseGrid");

    // DAM TODO need to figure out what to do for a sph or box psf

    ASKAPDEBUGASSERT(visData->shape().nelements()>=2);
    itsShape = scimath::PaddingUtils::paddedShape(visData->shape(),paddingFactor());

    initialiseCellSize(visData->axes());
    initStokes();
    initialiseFreqMapping();

    configureForPSF(dopsf);
    configureForPCF(dopcf);

    /// We only need one grid
    itsGrid.resize(1);
    itsGrid[0].resize(itsShape);
    itsGrid[0].set(0.0);

    // This ensures we reinitalise the pol converter
    itsVisPols.resize(0);

    initialiseSumOfWeights();
    ASKAPCHECK(itsSumWeights.nelements()>0, "Sum of weights not yet initialised");

    // extract the convolution kernel cache
    for (uint k = 0; k<visData->nPartitions(); ++k) {
        itsConvFunc = visData->griddingInfo(k)->getConvFunc();
        itsConvFuncOffsets = visData->griddingInfo(k)->getConvFuncOffsets();
    }

}

void VisGridder::initialiseDegrid(const VisData::ShPtr& visData, const casacore::Array<imtype>& in) {
    ASKAPTRACE("VisGridder::initialiseDegrid");
    ASKAP_LOGGER(logger, ".VisGridder.initialiseDegrid");
    itsShape = scimath::PaddingUtils::paddedShape(in.shape(),paddingFactor());

    initialiseCellSize(visData->axes());
    initStokes();
    initialiseFreqMapping();

    configureForPSF(false);
    configureForPCF(false);

    /// We only need one grid
    itsGrid.resize(1);
    itsGrid[0].resize(itsShape);

    // Make sure we reinitalise the pol converter
    itsVisPols.resize(0);

    if (casacore::max(casacore::abs(in))>0.0) {
        itsModelIsEmpty=false;
        casacore::Array<imtype> scratch(itsShape,0.);
        scimath::PaddingUtils::extract(scratch, paddingFactor()) = in;
        correctConvolution(scratch);
        casacore::Array<imtypeComplex> scratch2(itsGrid[0].shape());
        toComplex(scratch2, scratch);
        // @todo FIXME there is a race condition with spectral scatters. Leaving prints here until this is sorted
        ASKAPLOG_DEBUG_STR(logger, "calling fft2d");
        fft2d(scratch2, true);
        ASKAPLOG_INFO_STR(logger, "done");
        casacore::convertArray<casacore::Complex,imtypeComplex>(itsGrid[0],scratch2);
    } else {
        ASKAPLOG_DEBUG_STR(logger, "No need to degrid: model is empty");
        itsModelIsEmpty=true;
        itsGrid[0].set(casacore::Complex(0.0));
    }

    for (uint k = 0; k<visData->nPartitions(); ++k) {
        visData->griddingInfo(k)->initModel();
        itsConvFunc = visData->griddingInfo(k)->getConvFunc();
        itsConvFuncOffsets = visData->griddingInfo(k)->getConvFuncOffsets();
    }

}

/// @brief helper method to set up cell size
/// @details Similar action is required to calculate uv-cell size for gridding and degridding.
/// Moreover, derived gridders may override initialiseGrid and initialiseDegrid and we don't want
/// to duplicate the code up there. This method calculates uv-cell size for both ra and dec axes
/// using coordinate information provided. This method also assigns passed axes parameter to itsAxes.
/// @param[in] axes coordinate system (ra and dec axes are used).
void VisGridder::initialiseCellSize(const scimath::Axes& axes)
{
    ASKAP_LOGGER(logger, ".VisGridder.initialiseCellSize");
    itsAxes=axes;
    ASKAPCHECK(itsAxes.hasDirection(), "Direction axis is missing. itsAxes:"<<itsAxes);
    casacore::Vector<casacore::Double> increments = itsAxes.directionAxis().increment();
    ASKAPCHECK(increments.nelements() == 2, "Expect 2 elements in the increment vector, you have "<<increments);
    itsUVCellSize.resize(2);
    ASKAPDEBUGASSERT(itsShape.nelements()>=2);
    for (size_t dim = 0; dim<2; ++dim) {
         itsUVCellSize[dim] = 1./(increments[dim]*double(itsShape[dim]));
    }
}


/// @brief initialise sum of weights
/// @details We keep track the number of times each convolution function is used per
/// channel and polarisation (sum of weights). This method is made virtual to be able
/// to do gridder specific initialisation without overriding initialiseGrid.
/// This method accepts no parameters as itsShape, itsNWPlanes, etc should have already
/// been initialised by the time this method is called.
void VisGridder::initialiseSumOfWeights()
{
  resizeSumOfWeights(1);
  zeroSumOfWeights();
}

/// @brief resize sum of weights
/// @details This method is used inside initialiseSumOfWeights and its overrides in
/// derived classes. It resizes itsSumWeights to a given number of convolution
/// functions taking into account channels/polarisations according to itsShape.
/// Moving this operation into the separate method allows better data encapsulation
/// and tracking weights per oversampling plane or per convolution function depending
/// on the user's choice.
/// @param[in] numcf number of convolution functions in the cache (before oversampling)
void VisGridder::resizeSumOfWeights(const int numcf)
{
  ASKAPDEBUGASSERT(numcf>0);
  const int numRows = itsTrackWeightPerOversamplePlane ? numcf*itsOverSample*itsOverSample : numcf;
  ASKAPDEBUGASSERT(numRows>0);
  itsSumWeights.resize(numRows,itsShape.nelements()>=3 ? itsShape(2) : 1,
                         itsShape.nelements()>=4 ? itsShape(3) : 1);
}

/// This is the default implementation
void VisGridder::finaliseGrid(casacore::Array<imtype>& out) {
    ASKAPTRACE("VisGridder::finaliseGrid");
    ASKAP_LOGGER(logger, ".VisGridder.finaliseGrid");
    ASKAPDEBUGASSERT(itsGrid.size() > 0);
    // buffer for result
    casacore::Array<imtype> dBuffer(itsGrid[0].shape());
    ASKAPDEBUGASSERT(dBuffer.shape().nelements()>=2);
    ASKAPDEBUGASSERT(itsShape == scimath::PaddingUtils::paddedShape(out.shape(),paddingFactor()));

    /// Loop over all grids Fourier transforming and accumulating
    for (unsigned int i=0; i<itsGrid.size(); i++) {
        #ifdef ASKAP_FLOAT_IMAGE_PARAMS
        casacore::Array<imtypeComplex> scratch;
        if (itsClearGrid) {
            // we'll throw the grid away at the end, so we can use it as scratch space
            scratch.reference(itsGrid[i]);
        } else {
            // we want the pre-fft grid, make a copy
            scratch = itsGrid[i];
        }
        #else
        casacore::Array<imtypeComplex> scratch(itsGrid[i].shape());
        casacore::convertArray<imtypeComplex,casacore::Complex>(scratch, itsGrid[i]);
        #endif

        // @todo FIXME there is a race condition with spectral scatters. Leaving prints here until this is sorted
        ASKAPLOG_DEBUG_STR(logger, "calling fft2d");
        fft2d(scratch, false);
        ASKAPLOG_INFO_STR(logger, "done");
        if (i==0) {
            toDouble(dBuffer, scratch);
        } else {
            casacore::Array<imtype> work(dBuffer.shape());
            toDouble(work, scratch);
            dBuffer+=work;
        }
    }
    // Now we can do the convolution correction
    correctConvolution(dBuffer);
    dBuffer*=imtype(double(dBuffer.shape()(0))*double(dBuffer.shape()(1)));
    out = scimath::PaddingUtils::extract(dBuffer,paddingFactor());

    // Free up the grid memory?
    if (itsClearGrid) {
        ASKAPLOG_INFO_STR(logger,"Clearing the grid");
        itsGrid.resize(0);
        its2dGrid.resize(0,0);
        itsGridIndex=-1;
    }
}

/// @brief store given grid
/// @details This is a helper method for debugging, it stores the amplitude of a given
/// grid into a CASA image (prior to FFT done as part of finaliseGrid)
/// @param[in] name image name
/// @param[in] numGrid number of the grid to store
void VisGridder::storeGrid(const std::string &name, casacore::uInt numGrid) const
{
   ASKAPCHECK(numGrid < itsGrid.size(), "Requested grid number of "<<numGrid<<
              " exceeds or equal to "<<itsGrid.size()<<", the number of grids available");
   casacore::Array<float> buffer(itsGrid[numGrid].shape());
   buffer = amplitude(itsGrid[numGrid]);
   scimath::saveAsCasaImage(name, buffer);
}



/// This is the default implementation
void VisGridder::finaliseWeights(casacore::Array<imtype>& out) {
   ASKAPTRACE("VisGridder::finaliseWeights");
   ASKAPDEBUGASSERT(itsShape.nelements() >= 4);
    ASKAPDEBUGASSERT(itsShape == scimath::PaddingUtils::paddedShape(out.shape(),paddingFactor()));

    int nPol=itsShape(2);
    int nChan=itsShape(3);
    ASKAPDEBUGASSERT(out.shape().nelements() == 4);
    casa::IPosition ipStart(4, 0, 0, 0, 0);
    casa::IPosition ipEnd(4, out.shape()(0)-1, out.shape()(1)-1, 0, 0);

    ASKAPCHECK(itsSumWeights.nelements()>0, "Sum of weights not yet initialised");
    int nZ=itsSumWeights.shape()(0);

    for (int chan=0; chan<nChan; chan++) {
        ipStart(3) = chan;
        ipEnd(3) = chan;
        for (int pol=0; pol<nPol; pol++) {
            double sumwt=0.0;
            for (int iz=0; iz<nZ; iz++) {
              //              float sumConvFunc=real(casacore::sum(casacore::abs(itsConvFunc[iz])));
              //              ASKAPLOG_DEBUG_STR(logger, "Sum of conv func " << sumConvFunc);
                sumwt+=itsSumWeights(iz, pol, chan);
            }
            ipStart(2) = pol;
            ipEnd(2) = pol;
            out(ipStart,ipEnd).set(sumwt);
        }
    }
}

/// @brief helper method to initialise frequency mapping
/// @details Derived gridders may override initialiseGrid and initialiseDegrid. Howerver,
/// they still need to be able to initialise frequency axis mapping (between accessor channels
/// and image cube), which is handled by a private member class. This method initialises the
/// mapper using the content of itsShape and itsAxes, which should be set prior to calling this
/// method.
void VisGridder::initialiseFreqMapping()
{
    ASKAP_LOGGER(logger, ".VisGridder.initialiseFreqMapping");
    if (itsAxes.has("FREQUENCY") && itsShape.nelements()>=4) {
        itsFreqMapper.setupImage(itsAxes, itsShape(3));
    } else {
        ASKAPLOG_DEBUG_STR(logger, "Forced to use single spectral plane gridding (either "
                                  "FREQUENCY axis or the number of channels are missing");
        itsFreqMapper.setupSinglePlaneGridding();
    }
}

/// @brief log unused spectral planes
/// @details It is handy to write down the channel numbers into log if the sum of weights
/// is zero, i.e. if we have no data for this particular channel. This method does it
/// (it is called from the destructor, unless the gridder object didn't do any gridding).
void VisGridder::logUnusedSpectralPlanes() const
{
    ASKAP_LOGGER(logger, ".VisGridder.logUnusedSpectralPlanes");
   if (itsSumWeights.nelements()>0) {
       std::string planesList;
       for (casacore::uInt plane = 0; plane<itsSumWeights.nplane(); ++plane) {
            const double sumWt = casacore::sum(itsSumWeights.xyPlane(plane));
            if (sumWt<=0.) {
                if (planesList.size()!=0) {
                    planesList+=",";
                }
                planesList += utility::toString<casacore::uInt>(plane);
            }
       }
       if (planesList.size() == 0) {
           planesList = "none";
       }
       ASKAPLOG_DEBUG_STR(logger, "Unused spectral planes: "<<planesList);
   }
}


/// This is the default implementation
void VisGridder::finaliseDegrid() {
    /// Nothing to do
    // Free up the grid memory?
    if (itsClearGrid) {
        itsGrid.resize(0);
        its2dGrid.resize(0,0);
        itsGridIndex=-1;
    }
}

// This ShPtr should get deep-copied during cloning.
void VisGridder::initVisWeights(const askap::synthesis::IVisWeights::ShPtr &viswt)
{
    itsVisWeight = viswt;
}

// Customize for the specific type of Visibility weight.
// Input string is whatever is after "image" => "image.i.0.xxx" gives ".i.0.xxx "
void VisGridder::customiseForContext(const std::string &context)
{
    ASKAP_LOGGER(logger, ".VisGridder.customiseForContext");

    // RVU : Set up model dependant gridder behaviour
    //       For MFS, gridders for each Taylor term need different VisWeights.
    //  parse the 'context' string, and generate the "order" parameter.
    ASKAPLOG_DEBUG_STR(logger, "Customising gridder for context " << context);
    /*
    char corder[2];
    corder[0] = *(context.data()+3); // read the fourth character to get the order of the Taylor coefficient.
    corder[1] = '\n';
    int order = atoi(corder);
    //    ASKAPLOG_DEBUG_STR(logger, "Customising gridder for context " << context
    //              << " corder " << corder << " order" << order);
    if(order <0 || order >9) order = 0;
    */
    // MV: more general version
    const askap::synthesis::ImageParamsHelper iph(context);
    const int order = iph.isTaylorTerm() ? iph.order() : 1;

    if(itsVisWeight) {
        itsVisWeight->setParameters(order);
    }
}

/// This is the default implementation
int VisGridder::cIndex(int /*sample*/) {
    return 0;
}

/// This is the default implementation
int VisGridder::gIndex(int /*sample*/) {
    return 0;
}

/// @brief Obtain offset for the given convolution function
/// @details To conserve memory and speed the gridding up, convolution functions stored in the cache
/// may have an offset (i.e. essentially each CF should be defined on a bigger support and placed at a
/// pixel other than the centre of this support). This method returns this offset, which is the
/// position of the peak of the given CF on a bigger support w.r.t. the centre of this support.
/// The value of (0,0) means no offset from the centre (i.e. support is already centred).
/// @param[in] cfPlane plane of the convolution function cache to get the offset for
/// @return a pair with offsets for each axis
/// @note if there is no offset defined for a given cfPlane (default behavior), this method returns (0,0)
std::pair<int,int> VisGridder::getConvFuncOffset(int cfPlane) const
{
  ASKAPDEBUGASSERT(cfPlane>=0);
  if (cfPlane >= int(itsConvFuncOffsets.size())) {
      return std::pair<int,int>(0,0);
  }
  return itsConvFuncOffsets[cfPlane];
}

/// @brief initialise convolution function offsets for a given number of planes
/// @details The vector with offsets is resized and filled with (0,0).
/// @param[in] nPlanes number of planes in the cache
void VisGridder::initConvFuncOffsets(size_t nPlanes)
{
  itsConvFuncOffsets.resize(nPlanes);
  for (std::vector<std::pair<int,int> >::iterator it = itsConvFuncOffsets.begin(); it != itsConvFuncOffsets.end(); ++it) {
       it->first = 0;
       it->second = 0;
  }
}

/// @brief Assign offset to a particular convolution function
/// @details
/// @param[in] cfPlane plane of the convolution function cache to assign the offset for
/// @param[in] x offset in the first coordinate
/// @param[in] y offset in the second coordinate
/// @note For this method, cfPlane should be within the range [0..nPlanes-1].
void VisGridder::setConvFuncOffset(int cfPlane, int x, int y)
{
  ASKAPDEBUGASSERT(cfPlane>=0);
  ASKAPCHECK(cfPlane < int(itsConvFuncOffsets.size()), "An attempt to set offset for plane (before oversampling) "<<
             cfPlane<<" while the buffer has been initialised to handle "<<itsConvFuncOffsets.size()<<" planes only");
  itsConvFuncOffsets[cfPlane] = std::pair<int,int>(x,y);
}

/// @brief check whether the model is empty
/// @details A simple check allows us to bypass heavy calculations if the input model
/// is empty (all pixels are zero). This makes sense for degridding only.
/// @brief true, if the model is empty
bool VisGridder::isModelEmpty() const
{
  return itsModelIsEmpty;
}

}

