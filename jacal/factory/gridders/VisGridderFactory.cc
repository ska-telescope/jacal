/// @file VisGridderFactory.cc
///
/// @brief Factory class for visibility gridders.
/// @details This is a cutdown version of the Yandasoft factory

// for logging
#define ASKAP_PACKAGE_NAME "VisGridderFactory"
#include <string>
/// askap namespace
namespace askap {
/// @return version of the package
    std::string getAskapPackageVersion_VisGridderFactory() {
        return std::string("VisGridderFactory; ASKAPsoft==Unknown");

    }
}
/// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_VisGridderFactory()

// Local includes
#include <factory/gridders/VisGridderFactory.h>
#include <factory/gridders/BoxVisGridder.h>
#include <factory/gridders/SphFuncVisGridder.h>
#include <factory/gridders/WProjectVisGridder.h>
#include <factory/gridders/AWProjectVisGridder.h>

// Base ASKAP
#include <askap/AskapLogging.h>
#include <askap/AskapError.h>
ASKAP_LOGGER(logger, ".gridding.VisGridderFactory");

// CASA includes
#include <casacore/casa/OS/DynLib.h>        // for dynamic library loading
#include <casacore/casa/BasicSL/String.h>   // for downcase
#include <casacore/scimath/Mathematics/Interpolate2D.h>

// Yandasoft includes
#include <gridding/VisWeightsMultiFrequency.h>
#include <measurementequation/SynthesisParamsHelper.h>

namespace askap {

  // Define the static registry.
  std::map<std::string, VisGridderFactory::GridderCreator*> VisGridderFactory::theirRegistry;

  VisGridderFactory::VisGridderFactory() {
  }

  void VisGridderFactory::registerGridder(const std::string& name, VisGridderFactory::GridderCreator* creatorFunc)
  {
    ASKAPLOG_DEBUG_STR(logger, "     - Adding "<<name<<" gridder to the registry");
    theirRegistry[name] = creatorFunc;
  }

  IVisGridder::ShPtr VisGridderFactory::createGridder(const std::string& name,
                                                                        const LOFAR::ParameterSet& parset)
  {
    std::map<std::string,GridderCreator*>::const_iterator it = theirRegistry.find (name);
    if (it == theirRegistry.end()) {
      // Unknown gridder. Try to load the data manager from a dynamic library
      // with that lowercase name (without possible template extension).
      std::string libname(toLower(name));
      const std::string::size_type pos = libname.find_first_of (".<");
      if (pos != std::string::npos) {
        libname = libname.substr (0, pos);      // only take before . or <
      }
      // Try to load the dynamic library and execute its register function.
      // Do not dlclose the library.
      //
      try {
          ASKAPLOG_INFO_STR(logger, "Gridder "<<name<<
                 " is not in the registry, attempting to load it dynamically");
          casacore::DynLib dl(libname, string("libaskap_"), "register_"+libname, false);
	  if (dl.getHandle()) {
              // Successfully loaded. Get the creator function.
              ASKAPLOG_INFO_STR(logger, "Dynamically loaded gridder " << name);
              // the first thing the gridder in the shared library is supposed to do is to
              // register itself. Therefore, its name will appear in the registry.
              it = theirRegistry.find (name);
          }

      }
      catch (std::exception) {
           ASKAPTHROW(AskapError, "Unknown gridder " << name);
      }

    }
    if (it == theirRegistry.end()) {
      ASKAPTHROW(AskapError, "Unknown gridder " << name);
    }
    // Execute the registered function.
    return it->second(parset);
  }

  // Make the gridder object for the gridder given in the parset file.
  // Currently the standard gridders are still handled by this function.
  // In the (near) future it should be done by putting creator functions
  // for these gridders in the registry and use that.
IVisGridder::ShPtr VisGridderFactory::make(const LOFAR::ParameterSet &parset) {
    if (theirRegistry.size() == 0) {
        // this is the first call of the method, we need to fill the registry with
        // all pre-defined gridders
        ASKAPLOG_DEBUG_STR(logger, "Filling the gridder registry with pre-defined gridders");
        addPreDefinedGridder<WProjectVisGridder>();
        addPreDefinedGridder<AWProjectVisGridder>();
        addPreDefinedGridder<BoxVisGridder>();
        addPreDefinedGridder<SphFuncVisGridder>();
    }

    // buffer for the result
    IVisGridder::ShPtr gridder;
    /// @todo Better handling of string case
    std::string prefix("gridder");
    const string gridderName = parset.getString(prefix);
    prefix += "." + gridderName + ".";
    ASKAPLOG_DEBUG_STR(logger, "Attempting to greate gridder "<<gridderName);
    gridder = createGridder(gridderName, parset.makeSubset(prefix));

    ASKAPASSERT(gridder);

    // Initialize the Visibility Weights
    if (parset.getString("visweights","")=="MFS")
    {
        double reffreq=parset.getDouble("visweights.MFS.reffreq", 1.405e+09);
        ASKAPLOG_INFO_STR(logger, "Initialising for MFS with reference frequency " << reffreq << " Hz");
        ASKAPLOG_INFO_STR(logger, "Assuming that the number of Taylor terms is greater than 1");

        // set itsRefFreq = reffreq and itsOrder = 0
        gridder->initVisWeights(askap::synthesis::IVisWeights::ShPtr(
            new askap::synthesis::VisWeightsMultiFrequency(reffreq)));
    }
    else // Null....
    {
        //		gridder->initVisWeights(askap::synthesis::IVisWeights::ShPtr(new VisWeightsMultiFrequency()));
    }

    return gridder;
}

} // namespace askap
