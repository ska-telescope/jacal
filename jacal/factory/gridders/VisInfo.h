/// @file VisInfo.h
///
/// @brief 
/// @details 
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

#ifndef ASKAP_FACTORY_VISINFO_H
#define ASKAP_FACTORY_VISINFO_H

// std includes
#include <complex>
#include <vector>

// Yandasoft includes
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>
#include <scimath/utils/PolConverter.h>

#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

namespace askap {

/// @brief Interface class for read-only access to visibility data
/// @ingroup dataaccess_i
class VisInfo
{

public:

    VisInfo();

    ~VisInfo();

    typedef boost::shared_ptr<VisInfo> ShPtr;

    /// add samples from a const data accessor
    /// @param acc a reference to the data accessor
    void addData(const accessors::IConstDataAccessor& acc);

    void copySamples(boost::shared_ptr<VisInfo> &in_ptr, const std::vector<size_t> &idx);

    /// Convert buffered vis data and weights to the imaging polarisation frame
    void convertPol(const casacore::Vector<casacore::Stokes::StokesTypes>& imagePol);

    /// The number of samples (like nRows in DataAccessors, but flattened with nChannels)
    /// @return the number of samples
    int nSample() {return itsNumberOfSamples;}

    /// @brief The number of polarisations
    /// @return the number of polarisations
    int nPol() {return itsNumberOfPols;}

    /// @brief The number of polarisations
    /// @return the number of polarisations
    int nChannel() {return itsNumberOfChannels;}

    /// @brief Track whether or not the vis and weights have been converted to the imaging polarisation frame
    /// @return true if they have been converted
    bool convertedToImagePol() {return itsConvertedToImagePol;}

    /// @brief Track whether or not the vis and weights have been converted to the imaging polarisation frame
    /// @return true if they have been converted
    void convertedToImagePol(const bool state) {itsConvertedToImagePol = state;}

    /// @brief Reset the number of polarisations
    void resetNPol() {itsNumberOfPols = itsStokes.size();}

    /// Return reference to metadata vectors
    std::vector<double> &u() {return itsU;}
    std::vector<double> &v() {return itsV;}
    std::vector<double> &w() {return itsW;}
    std::vector<std::complex<float> > &phasor() {return itsPhasor;}
    std::vector<int> &chan() {return itsChannel;}
    //std::vector<bool> &flag() {return itsFlag;}
    std::vector<int> &flag() {return itsFlag;}
    casacore::Vector<casacore::Stokes::StokesTypes> &stokes() {return itsStokes;}
    std::vector<double> &frequencyList() {return itsFrequencyList;}

    /// Return reference to beam metadata vectors
    std::vector<casacore::MVDirection> &pointingDir1() {return itsPointingDir1;}
    std::vector<casacore::MVDirection> &pointingDir2() {return itsPointingDir2;}
    std::vector<float> &feed1PA() {return itsFeed1PA;}
    std::vector<float> &feed2PA() {return itsFeed2PA;}

    /// @brief Return reference to the visibility noise vector
    /// @return the reference to the noise
    std::vector<std::vector<float> > &noise() {return itsNoise;}

    /// @brief Return reference to the visibility noise vector
    /// @return the reference to the noise
    std::vector<std::vector<float> > &weight() {return itsWeight;}

    /// @brief Return reference to the visibility data sample vector
    /// @return the reference to the samples
    std::vector<std::vector<std::complex<float> > > &sample() {return itsSample;}

    /// @brief Return reference to the visibility model vector
    /// @return the reference to the models
    std::vector<std::vector<std::complex<float> > > &model() {return itsModel;}

    /// @brief set the centre of the image
    /// @details This method extracts RA and DEC axes from itsAxes and
    /// forms a direction measure corresponding to the middle of each axis.
    /// @return direction measure corresponding to the image centre
    void setImageCentre(const casacore::MVDirection dir) {itsImageCentre = dir;}

    /// @brief set the tangent point
    /// @details For faceting all images should be constructed for the same tangent
    /// point. This method extracts the tangent point (reference position) from the
    /// coordinate system.
    /// @return direction measure corresponding to the tangent point
    void setTangentPoint(const casacore::MVDirection dir) {itsTangentPoint = dir;}

    /// Output shift operator for Params
    /// @param os Output ostream
    /// @param par Parameters to be processed
    friend LOFAR::BlobOStream& operator<<(LOFAR::BlobOStream& os, const VisInfo& gdp);
    /// Input shift operator for Params
    /// @param[in] is Input stream
    /// @param[in] par Parameters to be processed
    friend LOFAR::BlobIStream& operator>>(LOFAR::BlobIStream& is, VisInfo& gdp);

private:

    int itsNumberOfSamples;    // total number of baselines & frequencies in each partition
    int itsNumberOfPols;
    int itsNumberOfChannels;

    /// Track whether or not the vis and weights have been converted to the imaging polarisation frame
    bool itsConvertedToImagePol;

    /// Vectors of length itsNumberOfSamples
    std::vector<double> itsU;
    std::vector<double> itsV;
    std::vector<double> itsW;
    std::vector<std::complex<float> > itsPhasor;
    std::vector<int> itsChannel;
    // BlobIStream/BlobOStream treat std::vector<bool> separately, but it doesn't seem to be working. Use int for now
    //std::vector<bool> itsFlag;
    std::vector<int> itsFlag;

    /// Beam vectors of length itsNumberOfSamples
    std::vector<casacore::MVDirection> itsPointingDir1;
    std::vector<casacore::MVDirection> itsPointingDir2;
    std::vector<float> itsFeed1PA;
    std::vector<float> itsFeed2PA;

    /// Vectors of length itsNumberOfPols
    casacore::Vector<casacore::Stokes::StokesTypes> itsStokes;

    /// Vectors of length itsNumberOfChannels
    std::vector<double> itsFrequencyList;

    /// Nested vectors of length itsNumberOfSamples,itsNumberOfPols
    std::vector<std::vector<float> > itsWeight;
    std::vector<std::vector<float> > itsNoise;
    std::vector<std::vector<std::complex<float> > > itsSample;
    std::vector<std::vector<std::complex<float> > > itsModel;

    casacore::MVDirection itsImageCentre;
    casacore::MVDirection itsTangentPoint;

};

} // end of namespace askap

#endif // #ifndef ASKAP_FACTORY_VISINFO_H
