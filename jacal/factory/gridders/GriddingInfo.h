/// @file GriddingInfo.h
///
/// @brief 
/// @details 
///
/// @author Daniel Mitchell <daniel.mitchell@csiro.au>

#ifndef ASKAP_FACTORY_GRIDDINGINFO_H
#define ASKAP_FACTORY_GRIDDINGINFO_H

// std includes
#include <complex>
#include <vector>

// Yandasoft includes
#include <dataaccess/IDataIterator.h>
#include <dataaccess/SharedIter.h>
#include <scimath/utils/PolConverter.h>

#include <Blob/BlobOStream.h>
#include <Blob/BlobIStream.h>

namespace askap {

/// @brief Interface class for read-only access to visibility data
/// @ingroup dataaccess_i
class GriddingInfo
{

public:

    GriddingInfo();

    ~GriddingInfo();

    typedef boost::shared_ptr<GriddingInfo> ShPtr;

    /// add samples from a const data accessor
    /// @param acc a reference to the data accessor
    void addData(const accessors::IConstDataAccessor& acc);

    void copySamples(boost::shared_ptr<GriddingInfo> &in_ptr, const std::vector<size_t> &idx);

    void initModel();
    void subtractModel();
    void resetModel();

    /// Convert buffered vis data and weights to the imaging polarisation frame
    void convertPol(const casacore::Vector<casacore::Stokes::StokesTypes>& imagePol);

    /// get the number of samples (like nRows in DataAccessors, but flattened with nChannels)
    /// @return the number of samples
    int nSample() {return itsNumberOfSamples;}

    /// @brief set the number of samples
    /// @param the number of samples
    int nSample(const int n) {itsNumberOfSamples = n;}

    /// @brief get the number of polarisations
    /// @return the number of polarisations
    int nPol() {return itsNumberOfPols;}

    /// @brief set the number of polarisations
    /// @param set the number of polarisations
    int nPol(const int n) {itsNumberOfPols = n;}

    /// @brief get the number of frequency channels
    /// @return the number of frequency channels
    int nChannel() {return itsNumberOfChannels;}

    /// @brief set the number of polarisations
    /// @param set the number of polarisations
    int nChannel(const int n) {itsNumberOfChannels = n;}

    /// Return reference to metadata vectors
    std::vector<std::complex<float> > &phasor() {return itsPhasor;}
    std::vector<int> &chan() {return itsChannel;}
    //std::vector<bool> &flag() {return itsFlag;}
    std::vector<int> &flag() {return itsFlag;}
    casacore::Vector<casacore::Stokes::StokesTypes> &stokes() {return itsStokes;}
    std::vector<double> &frequencyList() {return itsFrequencyList;}

    /// Return reference to optional metadata vectors
    std::vector<int> &gridIndexU() {return itsGridIndexU;}
    std::vector<int> &gridIndexV() {return itsGridIndexV;}
    std::vector<int> &gridIndexW() {return itsGridIndexW;}
    std::vector<int> &kernelIndex() {return itsKernelIndex;}

    /// @brief Return reference to the visibility noise vector
    /// @return the reference to the noise
    std::vector<std::vector<float> > &weight() {return itsWeight;}

    /// @brief Return reference to the visibility data sample vector
    /// @return the reference to the samples
    std::vector<std::vector<std::complex<float> > > &sample() {return itsSample;}

    /// @brief Return reference to the visibility model vector
    /// @return the reference to the models
    std::vector<std::vector<std::complex<float> > > &model() {return itsModel;}

    /// @brief Update the visibility data sample vector
    /// @param reference to the new samples
    void updateVis(std::vector<std::vector<std::complex<float> > > &vis);

    /// @brief Set convolution function cache
    void setConvFunc(std::vector<casacore::Matrix<casacore::Complex> > &convFunc) {itsConvFunc = convFunc;}

    /// @brief Return convolution function cache
    std::vector<casacore::Matrix<casacore::Complex> > &getConvFunc() {return itsConvFunc;}

    /// @brief Set offsets of convolution functions
    void setConvFuncOffsets(std::vector<std::pair<int,int> > offsets) {itsConvFuncOffsets = offsets;}

    /// @brief Return offsets of convolution functions
    std::vector<std::pair<int,int> > getConvFuncOffsets() {return itsConvFuncOffsets;}

    /// Output shift operator for Params
    /// @param os Output ostream
    /// @param par Parameters to be processed
    friend LOFAR::BlobOStream& operator<<(LOFAR::BlobOStream& os,
        const GriddingInfo& gdp);
    /// Input shift operator for Params
    /// @param[in] is Input stream
    /// @param[in] par Parameters to be processed
    friend LOFAR::BlobIStream& operator>>(LOFAR::BlobIStream& is,
        GriddingInfo& gdp);

protected:

    /// @brief Obtain offset for the given convolution function
    /// @details To conserve memory and speed the gridding up, convolution functions stored in the cache
    /// may have an offset (i.e. essentially each CF should be defined on a bigger support and placed at a
    /// pixel other than the centre of this support). This method returns this offset, which is the
    /// position of the peak of the given CF on a bigger support w.r.t. the centre of this support.
    /// The value of (0,0) means no offset from the centre (i.e. support is already centred).
    /// @param[in] cfPlane plane of the convolution function cache to get the offset for
    /// @return a pair with offsets for each axis
    /// @note if there is no offset defined for a given cfPlane (default behavior), this method returns (0,0)
    std::pair<int,int> getConvFuncOffset(int cfPlane) const;

private:

    int itsNumberOfSamples;    // total number of baselines & frequencies in each partition
    int itsNumberOfPols;
    int itsNumberOfChannels;

    /// Vectors of length itsNumberOfSamples
    std::vector<std::complex<float> > itsPhasor;
    std::vector<int> itsChannel;
    // BlobIStream/BlobOStream treat std::vector<bool> separately, but it doesn't seem to be working. Use int for now
    //std::vector<bool> itsFlag;
    std::vector<int> itsFlag;

    /// Optional vectors of length itsNumberOfSamples
    std::vector<int> itsGridIndexU;
    std::vector<int> itsGridIndexV;
    std::vector<int> itsGridIndexW;
    std::vector<int> itsKernelIndex;

    /// Vectors of length itsNumberOfPols
    casacore::Vector<casacore::Stokes::StokesTypes> itsStokes;

    /// Vectors of length itsNumberOfChannels
    std::vector<double> itsFrequencyList;

    /// Nested vectors of length itsNumberOfSamples,itsNumberOfPols
    std::vector<std::vector<float> > itsWeight;
    std::vector<std::vector<std::complex<float> > > itsSample;
    std::vector<std::vector<std::complex<float> > > itsModel;

    /// @brief Convolution function
    /// The convolution function is stored as a vector of arrays so that we can
    /// use any of a number of functions. The index is calculated by cIndex.
    std::vector<casacore::Matrix<casacore::Complex> > itsConvFunc;

    /// @brief offsets of convolution functions
    /// @details To conserve memory and speed the gridding up, convolution functions stored in the cache
    /// may have an offset (i.e. essentially each CF should be defined on a bigger support and placed at a
    /// pixel other than the centre of this support). This data field defines this offset, which is the
    /// position of the peak of the given CF on a bigger support w.r.t. the centre of this support.
    /// The value of (0,0) means no offset from the centre (i.e. support is already centred). If the index
    /// of CF is beyond the size of this vector, (0,0) offset is assumed. By default this vector is empty,
    /// which means no offset.
    std::vector<std::pair<int,int> > itsConvFuncOffsets;

};

} // end of namespace askap

#endif // #ifndef ASKAP_FACTORY_GRIDDINGINFO_H
