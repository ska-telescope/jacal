.. jacal documentation master file, created by
   sphinx-quickstart on Thu Feb 20 09:18:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


|jcl|
#####

**J**\ oint **A**\ stronomy **CAL**\ ibration and
imaging software

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   installation
   usage_eagle
   usage_direct
   imaging_overview
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
