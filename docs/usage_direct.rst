Example Usage from the Command Line
###################################

.. default-domain:: cpp
.. namespace:: askap

In this page we briefly describe how to use |jcl| in a |dlg| graph.
This assumes you already :doc:`built <installation>` |jcl|.

Outline
=======

In this example we will replicate one of the unit tests run in the GitLab CI
pipeline, namely ``test_basic_imaging``. This test performs basic imaging on an
input MeasurementSet using the :class:`CalcNE` and :class:`SolveNE` |jcl|
components. The other unit tests work similarly, exercising different |jcl|
components in different modes of operation.

In |dlg| a program is expressed as a *graph*, with nodes listing applications,
and the data flowing through them. Graphs come in two flavours: *logical*,
expressing the logical constructs used in the program (including loops, gather
and scatter components), and *physical*, which is the fully-fledged version of
a logical graph after expanding all the logical constructs.

This test is expressed as a *logical graph*. After translation into a *physical
graph* it is submitted for execution to the |dlg| *managers*, which need to be
started beforehand. During execution one can monitor the progress of the
program via a browser.

Preparing the Graph
===================

This test needs a few inputs:

 * The `logical graph <https://gitlab.com/ska-telescope/ska-sdp-jacal/-/blob/master/jacal/test/daliuge/test_basic_imaging.json>`_.
 * A `parset <https://gitlab.com/ska-telescope/ska-sdp-jacal/-/blob/master/jacal/test/daliuge/test_basic_imaging.in>`_
   (parsets are text files containing configuration options, and are the
   configuration mechanism used throughout |ys|).
 * Some `input data <https://gitlab.com/ska-telescope/ska-sdp-jacal/-/blob/master/data/chan_1.ms.tar.gz>`_.

Put all three files above in a new directory, and then decompress the input
data::

 $> mkdir tmp
 $> cd tmp
 $> export TEST_WORKING_DIR=$PWD
 $> wget https://gitlab.com/ska-telescope/ska-sdp-jacal/-/raw/master/jacal/test/daliuge/test_basic_imaging.json?inline=false
 $> wget https://gitlab.com/ska-telescope/ska-sdp-jacal/-/raw/master/jacal/test/daliuge/test_basic_imaging.in?inline=false
 $> wget https://gitlab.com/ska-telescope/ska-sdp-jacal/-/raw/master/data/chan_1.ms.tar.gz?inline=false
 $> tar xf chan_1.ms.tar.gz
 $> PARSET=$PWD/test_basic_imaging.in

Next, some adjustments will need to be made to the graph so that the |jcl|
shared library can be found, and the parset is correctly read at runtime::

 $> sed -i "s|%JACAL_SO%|$PATH_TO_JACAL_SO|g; s|%PARSET%|$PARSET|g" test_basic_imaging.json

Starting |dlg|
==============

Firstly, one needs to start the |dlg| *managers*, the runtime entities in
charge of executing graphs. We will start two: the *Node Manager* (NM), in
charge of executing the graph, and a *Data Island Manager* (DIM), in charge of
managing one or more NMs. Note that starting the DIM is not strictly required,
but is done for completeness.

Start the managers each on a different terminal so you can see their outputs
independently. Also, to make the test simpler, start both in the same directory
where the downloaded files are placed::

 $> cd $TEST_WORKING_DIR
 $> dlg nm -v
 $> dlg dim -N 127.0.0.1 -v

Running
=======

To execute a graph we submit it to one of the |dlg| managers (in our case, the
DIM). Also, because we are starting from a logical graph, we need to transform
it into a physical graph that can be run on the deployed managers.

This can be done as follows::

 $> cd $TEST_WORKING_DIR
 $> cat test_basic_imaging.in \
    | dlg unroll-and-partition `# Logical -> Physical translation` \
    | dlg map                  `# Assign nodes to drops (i.e., schedule the graph)` \
    | dlg submit -w            `# Submit and wait until execution finishes`

Finally, connect to ``127.0.0.1:8000`` to see the graph running:

.. image:: images/dlg_runtime.png

Note that :class:`CalcNE` now supports new gridders with more flexible data
partitioning. This can be enabled with |jcl|-specific parset parameter
*Cimager.gridder.dataaccess=datapartitions* (set to *yandasoft* or leave unset
to use the |ys| data iterators and gridders). The type of partitioning
is set with |jcl|-specific parset parameter *Cimager.gridder.partitiontype*::

 GitHub: ICRAR/EAGLE-graph-repo (master): examples/jacal_CalcNElight.graph

