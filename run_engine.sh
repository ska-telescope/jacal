DOCKER_OPTS="\
--shm-size=1g --ipc=shareable \
--rm \
--name daliuge-engine \
-v /var/run/docker.sock:/var/run/docker.sock \
-p 5555:5555 -p 6666:6666 \
-p 8000:8000 -p 8001:8001 \
-p 8002:8002 -p 9000:9000 \
" 

case "$1" in
    "jacal")
        DLG_ROOT="/tmp/.dlg"
        echo "Running Engine development version in background..."
        mkdir -p ${DLG_ROOT}/workspace
        mkdir -p ${DLG_ROOT}/testdata
        mkdir -p ${DLG_ROOT}/code
        DOCKER_OPTS=${DOCKER_OPTS}" -v ${DLG_ROOT}:${DLG_ROOT} --env DLG_ROOT=${DLG_ROOT}"
        echo "docker run -td ${DOCKER_OPTS}  icrar/daliuge-engine:jacal"
        docker run -td ${DOCKER_OPTS}  icrar/daliuge-engine:jacal
#        docker run -td ${DOCKER_OPTS} icrar/dlg-engine:casa
        sleep 3
        ./start_local_managers.sh
        exit 1;;
    *)
        echo "Usage run_engine.sh <jacal>"
        exit 1;;
esac
