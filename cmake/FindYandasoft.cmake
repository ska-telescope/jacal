# - Try to find readline, a library for easy editing of command lines.
# Variables used by this module:
#  YANDASOFT_ROOT_DIR     - Readline root directory
# Variables defined by this module:
#  YANDASOFT_FOUND - system has YANDASOFT
#  YANDASOFT_INCLUDE_DIR  - the YANDASOFT include directory (cached)
#  YANDASOFT_INCLUDE_DIRS - the YANDASOFT include directories
#                          (identical to YANDASOFT_INCLUDE_DIR)
#  YANDASOFT_LIBRARY      - the YANDASOFT library (cached)
#  YANDASOFT_LIBRARIES    - the YANDASOFT library plus the libraries it 
#                          depends on

# Copyright (C) 2019

if (TARGET askap::yandasoft)
	set(YANDASOFT_FOUND TRUE)
	return()
endif()

# - yandasoft_find_library(_name)
#
# Search for the library ${_name}. 
# If library is found, add it to YANDASOFT_LIBRARIES
#
#   Usage: yandasoft_find_library(name)
#
macro(yandasoft_find_library _name)
  string(TOUPPER ${_name} _NAME)
  find_library(${_NAME}_LIBRARY NAMES ${_name}
	  PATHS ${YANDASOFT_ROOT_DIR} PATH_SUFFIXES lib)
  mark_as_advanced(${_NAME}_LIBRARY)
  if(${_NAME}_LIBRARY)
	  list(APPEND YANDASOFT_LIBRARIES ${${_NAME}_LIBRARY})
  endif(${_NAME}_LIBRARY)
endmacro(yandasoft_find_library _name)

# Define the Casacore components.
set(Yandasoft_components
  
  accessors
  analysis
  analysisutilities
  askap
  askapparallel 
  components
  cpcommon
  cpdataservices
  icewrapper
  imagemath
  ingest
  interfaces
  logfilters
  pipelinetasks
  scimath
  services
  vispublisher
 
)

foreach (_comp ${Yandasoft_components})
	yandasoft_find_library(askap_${_comp})
endforeach (_comp ${Yandasoft_components})

if(NOT YANDASOFT_FOUND)
	find_path(YANDASOFT_INCLUDE_DIR askap/Application.h
		HINTS ${YANDASOFT_ROOT_DIR} PATH_SUFFIXES askap)
	yandasoft_find_library(yanda_synthesis)
	set(YANDASOFT_INCLUDE_DIRS ${YANDASOFT_INCLUDE_DIR})
	message(STATUS "yandasoft include dir at ${YANDASOFT_INCLUDE_DIRS}")

endif(NOT YANDASOFT_FOUND)


# Print diagnostics.
message(STATUS "Found the following Yandasoft components: ")
foreach(_comp ${Yandasoft_components})
   string(TOUPPER askap_${_comp} _COMP)
   message(STATUS "  ${_comp}: ${${_COMP}_LIBRARY}")
endforeach(_comp ${Yandasoft_components})

